define([
        'jquery',
        
        'text!group/tpy/category/tmpl/TpyCategory_Main.html',
        
        'group/tpy/category/ctrl/TpyCategoryList',
        'group/tpy/category/ctrl/TpyCategoryEnt',
        
        ],
        function($,         		
        		TpyCategory_Main,
        		
        		TpyCategoryList, 
        		TpyCategoryEnt
        		
        ) {

	var TpyCategoryMain 	= function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;
		
		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;		
		//------------------------------------------------------------------------------------
		
		this.var_lc_MODE_INIT 		= 0;
		this.var_lc_MODE_NEW 		= 1; //duplicate is the mode new after clone object
		this.var_lc_MODE_MOD 		= 2;
		this.var_lc_MODE_DEL 		= 3;	
		this.var_lc_MODE_SEL 		= 5;
	
		this.var_lc_URL_Aut_Header	= null;
		//---------------------------------------------------------------
		const pr_TYPE_LEVEL_01      = 1;
		const pr_TYPE_LEVEL_02      = 2;
		const pr_TYPE_LEVEL_03      = 3;
		const pr_TYPE_LEVEL_04      = 4;
		
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			self.var_lc_URL_Aut_Header				= req_gl_Security_HttpHeader_Bearer (App.keys.KEY_STORAGE_CREDENTIAL);
			do_gl_refresh_SecuHeader (self);
			
			//---------------------------------------------------------------
			
			tmplName.TPY_CAT_MAIN					= "Main";
			tmplContr.do_lc_put_tmpl(tmplName.TPY_CAT_MAIN	, TpyCategory_Main); 
			
			//---------------------------------------------------------------
			
			if (!App.controller.TpyCategory)				
				 App.controller.TpyCategory				= {};
			
			if (!App.controller.TpyCategory.Main		)	
				App.controller.TpyCategory.Main 			= this; //important for other controller can get ref, when new this controller,
			
			if (!App.controller.TpyCategory.List		)  
				 App.controller.TpyCategory.List			= new TpyCategoryList				("#div_List", "#div_List_Header", "#div_List_Content");				
			if (!App.controller.TpyCategory.Ent			)  
				 App.controller.TpyCategory.Ent				= new TpyCategoryEnt				(null, "#div_Ent", null);
		

			App.controller.TpyCategory.List			.do_lc_init();
			App.controller.TpyCategory.Ent			.do_lc_init();
			//--------------------------------------------------------------------------------------------------
		}
		
		
		this.do_lc_show = function(sharedJson){
			try { 
				
				$(pr_divContent)		.html(tmplContr.req_lc_compile_tmpl(tmplName.TPY_CAT_MAIN, {}));	
//				do_gl_apply_right($(pr_divContent));
				
//				
				App.controller.TpyCategory.List	.do_lc_show();
				App.controller.TpyCategory.Ent	.do_lc_show(null);//init: obj is null	
							
			}catch(e) {				
				do_gl_send_exception(App.path.BASE_URL_API, self.var_lc_URL_Aut_Header, App.network, "Mcq.Group", "TpyCategoryMain", "do_lc_show", e.toString()) ;
			}
//			do_gl_enhance_within($(pr_divContent));
		};
		
		this.do_lc_binding_pages = function(div, options) {
			try {
				if(div.length>0) do_gl_enhance_within(div, options);
			} catch (e) {
				console.log(e);
				self.do_show_Msg(null, e);
			}
		};
		
		this.do_verify_user_right_soc_manage = function(){
			for(var i = 0; i< this.pr_right_soc_manage.length; i++){
				if(App.data.user.rights.includes(this.pr_right_soc_manage[i]))
					return true;
			}
			return false;
		}

		//---------private-----------------------------------------------------------------------------
		this.do_Get_Category_Values= function(){
			var ref 		= req_gl_Request_Content_Send('ServiceTpyCategory', 'SVTpyCategoryLst');			
			ref['parType'	]	= self.var_lc_TPY_CAT
			var fSucces		= [];		
			fSucces.push(req_gl_funct(null, do_Get_Lst_Cfg_Group_Response, []));	
//			fSucces.push(req_gl_funct(null, self.do_lc_show, []));
			
			var fError 		= req_gl_funct(App, self.do_show_Msg, [$.i18n("common_err_ajax")]);	
			
			App.network.do_lc_ajax_bg (App.path.BASE_URL_API, self.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;	
		}	
		
		var do_Get_Lst_Cfg_Group_Response = function(sharedJson) {
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
				let data = sharedJson[App['const'].RES_DATA];
				
				let lstMatCatI 		= [];
				let lstMatCatII 	= [];
				let lstMatCatIII 	= [];
				let lstMatCatIV 	= [];
				
				for(let i = 0; i < data.length; i ++){
					switch (data[i].typ01){
					case pr_TYPE_LEVEL_01:
						lstMatCatI.push(data[i]);
						break;
					case pr_TYPE_LEVEL_02:
						lstMatCatII.push(data[i]);
						break;
					case pr_TYPE_LEVEL_03:
						lstMatCatIII.push(data[i]);
						break;
					case pr_TYPE_LEVEL_04:
						lstMatCatIV.push(data[i]);
						break;
					default:
						break;
					}
				}
				
			App.data["MatCatsI"] 	= lstMatCatI;
			App.data["MatCatsII"] 	= lstMatCatII;
			App.data["MatCatsIII"] 	= lstMatCatIII;
			App.data["MatCatsIV"] 	= lstMatCatIV;
				
			}
			else{
			}
		}
		
		//--------------------------------------------------------------------------------------------
		this.do_show_Notify_Msg  = function (sharedJson, msg, typeNotify, modeOld, modeNew){
			if (typeNotify){				
				if (typeNotify == 1 ){
					if (!msg) msg = "OK";
					do_gl_show_Notify_Msg_Success 	(msg);
				} else if (typeNotify == 0){
					if (!msg) msg = "Err";
					do_gl_show_Notify_Msg_Error 	(msg);
				}
			}else{
				var code 		= sharedJson[App['const'].SV_CODE];
				var pr_ctr_Main	= App.controller.TpyCategory.Main;
				if(code == App['const'].SV_CODE_API_YES) {
					
					if (modeOld == pr_ctr_Main.var_lc_MODE_NEW || modeOld == pr_ctr_Main.var_lc_MODE_MOD){
						if (!msg){
							msg = $.i18n('common_ok_msg_save');
						}
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_DEL){
						if (!msg) msg = $.i18n('common_ok_msg_del');		  
					}
					
					do_gl_show_Notify_Msg_Success 	(msg);
					
				}else if (code== App['const'].SV_CODE_API_NO){
					if (modeOld == pr_ctr_Main.var_lc_MODE_INIT || modeOld == pr_ctr_Main.var_lc_MODE_SEL){
						if (!msg) msg = $.i18n('common_err_msg_get');		        		
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_NEW || modeOld == pr_ctr_Main.var_lc_MODE_MOD){
						if (!msg) msg = $.i18n('common_err_msg_save');		        		
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_DEL){
						if (!msg) msg = $.i18n('common_err_msg_del');		  
					}
					
					do_gl_show_Notify_Msg_Error 	(msg);
					
	        	} else {
	        		if (!msg) msg = $.i18n('common_err_msg_unknow');
	        		do_gl_show_Notify_Msg_Error 	(msg);
	        	}	
			}			
		}

		//--------------------------------------------------------------------------------------------
		this.do_show_Msg= function(sharedJson, msg){
			//alert(msg);
			console.log("do_show_Msg::" + msg);
		}		
			
	};

	return TpyCategoryMain;
  });