define([
        'jquery',
        'text!group/tpy/category/tmpl/TpyCategory_List.html',
        'text!group/tpy/category/tmpl/TpyCategory_List_Material.html',
        'text!group/tpy/category/tmpl/TpyCategory_List_Blog.html'
        
        ],
        function($, 
        		TpyCategory_List,
        		TpyCategory_List_Material,
        		TpyCategory_List_Blog
        ) 
        {

	var TpyCategoryList 	= function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;
		
		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		
		//------------------------------------------------------------------------------------
		var pr_SERVICE_CLASS		= "ServiceTpyCategoryDyn"; //to change by your need
		
		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntHeader 		= null;
		var pr_ctr_EntBtn 			= null;
		var pr_ctr_EntTabs 			= null;		
		
		//-----------------------------------------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			
			tmplName.TPY_CAT_LIST						= "List";
			tmplName.TPY_CAT_LIST_MAT					= "List_Mateial";
			tmplName.TPY_CAT_LIST_BLOG					= "List_Blog";
			
			tmplContr.do_lc_put_tmpl(tmplName.TPY_CAT_LIST		, TpyCategory_List); 
			tmplContr.do_lc_put_tmpl(tmplName.TPY_CAT_LIST_MAT	, TpyCategory_List_Material); 
			tmplContr.do_lc_put_tmpl(tmplName.TPY_CAT_LIST_BLOG	, TpyCategory_List_Blog); 
			
			//--------------------------------------------------------------------
			pr_ctr_Main 			= App.controller.TpyCategory.Main;
			pr_ctr_List 			= App.controller.TpyCategory.List;
			pr_ctr_Ent				= App.controller.TpyCategory.Ent;
		}
		
		//---------show-----------------------------------------------------------------------------
		var var_lc_TPY_CAT_MAT 		= 100;
		var var_lc_TPY_CAT_BLOG 	= 200;
		this.do_lc_show = function(){               
			try{
				$("#div_List")						.html(tmplContr.req_lc_compile_tmpl(tmplName.TPY_CAT_LIST		, {}));
			
				self.do_lc_show_byType(var_lc_TPY_CAT_MAT);
				self.do_lc_show_byType(var_lc_TPY_CAT_BLOG);
				
				//---------------------------------------------------------------
				App.controller.DBoard.DBoardMain.do_lc_bind_event_div_Minimize();
			}catch(e) {	
				console.log(e);
				do_gl_send_exception(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, App.network, "Mcq.group", "TpyCategoryList", "do_lc_show", e.toString()) ;
			}
		};
		
		this.do_lc_show_byType = function(type){
			if (type==var_lc_TPY_CAT_MAT){
				$("#div_List_Material") 		.html(tmplContr.req_lc_compile_tmpl(tmplName.TPY_CAT_LIST_MAT	, {}));
				do_get_list_ByAjax_Dyn("#div_TpyCat_Mat", var_lc_TPY_CAT_MAT);
			}else if (type==var_lc_TPY_CAT_BLOG){
				$("#div_List_Blog")				.html(tmplContr.req_lc_compile_tmpl(tmplName.TPY_CAT_LIST_BLOG	, {}));
				do_get_list_ByAjax_Dyn("#div_TpyCat_Blog", var_lc_TPY_CAT_BLOG);
			}
		}
		
		//--------------------------------------------------------------------------------------------
		var do_get_list_ByAjax_Dyn = function(div, type){	
			var ref 				= req_gl_Request_Content_Send(pr_SERVICE_CLASS, "SVLstDyn");
			if(type)	ref.parTyp 	= type; //type00
			
			var lang = localStorage.language;
			if (lang ==null ) lang 	= "en";	
			var filename 			= "www/js/lib/datatables/datatable_"+lang+".json";
			
			var additionalConfig 	= {	};
			
			var colConfig			= req_gl_table_col_config($(div).find("table"), null, additionalConfig);
			var dataTableOption 	= {
					"canScrollY"			: true,
					"searchOption" 			: true,	
					"searchOptionConfig" 	: [	{lab: $.i18n('common_search_any')			, val:0}]
			};	     		
			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Msg, [$.i18n("common_err_ajax")]);	
			
			//call Datatable AJAX dynamic function from DatatableTool
			var oTable 		= req_gl_Datatable_Ajax_Dyn		(div, App.path.BASE_URL_API,pr_ctr_Main.var_lc_URL_Aut_Header, filename, colConfig, ref, fError, undefined, null, undefined, do_bind_list_event, dataTableOption);
		}
		
		//--------------------------------------------------------------------------------------------
		var do_bind_list_event = function(sharedJson, div, oTable) {
			do_gl_enhance_within($(div), {div: div});
			
			$(div).find('tr').off('click')
			$(div).find('tr').on('click', function(){
				if(App.data.mode == pr_ctr_Main.var_lc_MODE_MOD || App.data.mode == pr_ctr_Main.var_lc_MODE_NEW){
					do_gl_show_Notify_Msg_Error ($.i18n('common_err_msg_sel'));
					return;
				}
				//apply CSS style
				do_gl_Add_Class_List($(this).parent(), $(this), "selected");
				
				var oData = oTable.fnGetData(this);
				pr_ctr_Ent. do_lc_show_ById(oData, pr_ctr_Main.var_lc_MODE_SEL);
		
			});
		};
	
	};

	return TpyCategoryList;
  });