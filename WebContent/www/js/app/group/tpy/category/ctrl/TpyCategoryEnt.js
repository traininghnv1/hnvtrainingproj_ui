define([
	'jquery',
	'text!group/tpy/category/tmpl/TpyCategory_Ent.html',
	
	'group/tpy/category/ctrl/TpyCategoryEntHeader',
    'group/tpy/category/ctrl/TpyCategoryEntBtn' 

	],
	function($, 
			TpyCategory_Ent,
			
			TpyCategoryEntHeader, 
    		TpyCategoryEntBtn
    ) {


	var TpyCategoryEnt     = function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;

		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;

		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 						= this;		
		//------------------------------------------------------------------------------------
		var pr_TYP_PARENT_CAT_MARKET= 15200;// change to adapt with back office for lock tool

		var pr_SERVICE_CLASS		= "ServiceTpyCategory"; //to change by your need

		var pr_SV_GET				= "SVGet"; 
		var pr_SV_NEW				= "SVNew"; 
		var pr_SV_DEL				= "SVDel"; 

		var pr_SV_MOD				= "SVMod"; 	//if not use lock

		var pr_SV_LCK_NEW			= "SVLckReq"; 
		var pr_SV_LCK_END			= "SVLckEnd"; 
		var pr_SV_LCK_DEL			= "SVLckDel"; 

		var pr_SV_DUPLICATE			= "SVDuplicate"; 
		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntHeader 		= null;
		var pr_ctr_EntBtn 			= null;
		var pr_ctr_EntTabs 			= null;


		//-----------------------------------------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		var pr_lock					= null;
		
		var pr_default_new_line	= {
				id 			: null,
				name		: null,
				lang		: null,	
		};
		
		var var_lc_TPY_CAT_MAT 		= 100;
		var var_lc_TPY_CAT_BLOG 	= 200;
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			tmplName.TPY_CAT_ENT						= "Ent";
			tmplContr.do_lc_put_tmpl(tmplName.TPY_CAT_ENT	, TpyCategory_Ent); 
			
			//--------------------------------------------------------------------
			if (!App.controller.TpyCategory.EntBtn		)  
				 App.controller.TpyCategory.EntBtn			= new TpyCategoryEntBtn				(null, "#div_Ent_Btn", null);
			if (!App.controller.TpyCategory.EntHeader	)  
				 App.controller.TpyCategory.EntHeader		= new TpyCategoryEntHeader			(null, "#div_Ent_Header", null);
			App.controller.TpyCategory.EntBtn		.do_lc_init();
			App.controller.TpyCategory.EntHeader	.do_lc_init();
			
			//--------------------------------------------------------------------
			pr_ctr_Main 			= App.controller.TpyCategory.Main;
			pr_ctr_List 			= App.controller.TpyCategory.List;

			pr_ctr_Ent				= App.controller.TpyCategory.Ent;
			pr_ctr_EntHeader 		= App.controller.TpyCategory.EntHeader;
			pr_ctr_EntBtn 			= App.controller.TpyCategory.EntBtn;
//			pr_ctr_EntTabs 			= App.controller.TpyCat.EntTabs;

			
			
		}

		//---------show-----------------------------------------------------------------------------
		this.do_lc_show		= function(obj, mode){			
			pr_object 	= obj;
			pr_mode		= !mode?pr_ctr_Main.var_lc_MODE_INIT: mode;

					

			if (pr_mode == pr_ctr_Main.var_lc_MODE_INIT){
				$("#div_Ent").html(tmplContr.req_lc_compile_tmpl(tmplName.TPY_CAT_ENT, {mode : pr_ctr_Main.var_lc_MODE_INIT}));
				pr_ctr_EntBtn		.do_lc_show(obj, pr_ctr_Main.var_lc_MODE_INIT);
				return;
			}

			if (!obj){
				$("#div_Ent").html(tmplContr.req_lc_compile_tmpl(tmplName.TPY_CAT_ENT, {mode: pr_ctr_Main.var_lc_MODE_INIT}));		
				pr_ctr_EntBtn		.do_lc_show(null, pr_ctr_Main.var_lc_MODE_INIT);
			}else{
				$("#div_Ent").html(tmplContr.req_lc_compile_tmpl(tmplName.TPY_CAT_ENT, obj));				
				pr_ctr_EntHeader	.do_lc_show(obj, mode);
				pr_ctr_EntBtn		.do_lc_show(obj, mode);
//				pr_ctr_EntTabs		.do_lc_show(obj, mode);
			}	

			pr_ctr_Main.do_lc_binding_pages($("#div_Ent_Header"),{
				obj: obj
			});

			if(mode == pr_ctr_Main.var_lc_MODE_NEW) {

			} else if(mode == pr_ctr_Main.var_lc_MODE_MOD) {
				do_gl_enable_edit($(pr_divContent));
				$("#inp_tpy_cat_code").attr("disabled", "disabled");
			} else {
				do_gl_disable_edit($(pr_divContent));
			}
			
			do_init_detail_table(obj, mode);
			//-------------------------------------------------------------
			App.controller.DBoard.DBoardMain.do_lc_bind_event_div_Minimize();
			App.controller.DBoard.DBoardMain.do_lc_prevent_winClosing (mode);
		}

		//---show after ajax request---------------------------
		var do_show_Obj = function (sharedJson, mode, localObj){
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {					
				App.data.mode		= mode;
				if (localObj){
					self.do_lc_show(localObj, mode); 
				}else{
					var object = sharedJson[App['const'].RES_DATA];        		
					self.do_lc_show(object, mode);  
				}			     		
			} else if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_ERROR){
				do_gl_show_Notify_Msg_Error ($.i18n('common_err_msg_save'));
				return;
			}		
		}
		
		


		this.do_lc_show_ById = function(obj, mode){
			var svName 		= pr_SV_GET;
			if (pr_mode == pr_ctr_Main.var_lc_MODE_MOD){
				//not use lock
				svName = pr_SV_GET

				//use lock
				//svName = pr_SV_LCK_NEW
			}

			var ref 		= req_gl_Request_Content_Send(pr_SERVICE_CLASS, svName);			
			ref.id			= obj.id;
			ref["forced"]   = true;

			var fSucces		= [];		
			fSucces.push(req_gl_funct(null, do_show_Obj, [mode]));	

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;			
		}

		//---------------------------------------------new object-------------------------------------------
		this.do_lc_new = function() {
			
//			var newObj		 = $.extend(true, {}, pr_default_new_obj);		
			var newObj		 = {};
			//action mode
			App.data.mode 	= pr_ctr_Main.var_lc_MODE_NEW;
			
			self		.do_lc_show(newObj, App.data.mode);

//			if(!App.data.TpyCategoryList[pr_TYP_PARENT_CAT_MARKET])
//				App.data.TpyCategoryList[pr_TYP_PARENT_CAT_MARKET] = [];
//			var newObj		= {};		
//			//action mode
//			App.data.mode 	= pr_ctr_Main.var_lc_MODE_NEW;
////			newObj.code		= "EDU_"+ reg_gl_DateStr_From_DateObj (new Date(), "yyMMddHHmmss");
//			newObj.parTyp	= pr_TYP_PARENT_CAT_MARKET;
//
//			self		.do_lc_show(newObj, App.data.mode);
//			//pr_ctr_Ent	.do_lc_show(newObj, App.data.mode);
//			do_Enabled_Edit();
		}

		//---------------------------------------------clone object-------------------------------------------
		this.do_lc_duplicate = function (obj){
			var newObj 	= $.extend(true, {}, obj);
			newObj.id	= null;
			App.data.mode 	= pr_ctr_Main.var_lc_MODE_NEW;

			self		.do_lc_show(newObj, App.data.mode);
			//pr_ctr_Ent	.do_lc_show(newObj, App.data.mode);
			do_Enabled_Edit();
		}

		//---------del obj-----------------------------------------------------------------------------
		this.do_lc_delete 		= function (obj){
			var ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_DEL);

			var lock 			= {};			
			lock.objectType 	= pr_TYP_PARENT_CAT_MARKET; 	//integer
			lock.objectKey 		= obj.id; 		//integer
			ref['lock'	]		= JSON.stringify(lock);
			ref["id"]			= obj.id;
			
			var fSucces			= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_DEL])); 
			fSucces.push(req_gl_funct(null	, do_show_Obj						, [pr_ctr_Main.var_lc_MODE_INIT]));	
			fSucces.push(req_gl_funct(null	, do_refresh_list					, [obj])); //refresh menu

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;			
		}


		//---------Lock-----------------------------------------------------------------------------
		this.do_lc_save		= function(obj, mode){	//save new object or save with lock		
			//to comeback on tab curent active
			do_gl_req_tab_active($(pr_divContent));
					
			if(!obj.files) obj.files = [];
			var	data = req_gl_data({
				dataZoneDom		: $("#div_Ent"),
				oldObject 		: {"files": obj.files},
				removeDeleted	: true				
			});
			
			//check data error
			if(data.hasError == true){
				do_gl_show_Notify_Msg_Error ($.i18n('tpy_cat_data_error_msg'));
				return false;
			}
			
			data.data.files		= obj.files;
			if (!data.data.files && data.data.logo) data.data.files = [];
			if (data.data.files && data.data.logo) 	data.data.files.push(data.data.logo);

			if(mode == pr_ctr_Main.var_lc_MODE_NEW){ 	
					do_send_new(data);
				}else if(mode == pr_ctr_Main.var_lc_MODE_MOD){
					//show msgbox for save and exit or save and continue
					App.MsgboxController.do_lc_show({
						title	: $.i18n("msgbox_confirm_title"),
						content : $.i18n("tpy_cat_msg_mod_save_content"),
						buttons	: {
							SAVE_EXIT: {
								lab		: $.i18n("common_btn_save_exit"),
								funct	: do_send_lock_end,
								param	: [data]							
							},
							SAVE_CONTINUE: {
								lab		:  $.i18n("common_btn_save_continue"),
								funct	: do_send_mod,
								param	: [data]	
							}
						}
					});	
				}else{
					//do_notify_error here
					return;
				}
		}

		this.do_lc_duplicate_category		= function(obj){	//save new object or save with lock		
			var data = req_gl_data({
				dataZoneDom : $("#div_tpy_cat_market_popup_duplicate_societe")
			});
			//check data error
			if(data.hasError) {
				do_gl_show_Notify_Msg_Error ($.i18n('tpy_cat_data_error_msg'));
				return;
			} else {
				do_send_duplicate_category(data);
			}
		}
		//-------------------------------------------New-------------------------------------------------------------
		var do_send_new = function(data) {
			var ref		= {};
			ref 		= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_NEW);

			var fSucces	= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_NEW])); 
			fSucces.push(req_gl_funct(null	, do_show_Obj						, [pr_ctr_Main.var_lc_MODE_SEL]));
			fSucces.push(req_gl_funct(null	, do_refresh_list					, [data.data]));

			var fError 	= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			data.do_lc_send_data(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, fSucces, fError, "obj");
			
		}

		var do_send_mod = function(data) {
			var ref 	= {};
			ref 		= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_MOD);

			var fSucces	= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_MOD])); 
			fSucces.push(req_gl_funct(null	, do_show_Obj						, [pr_ctr_Main.var_lc_MODE_MOD]));
			fSucces.push(req_gl_funct(null	, do_refresh_list					, [data.data]));
			fSucces.push(req_gl_funct(null	, do_Enabled_Edit					, []));


			var fError 	= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			data.do_lc_send_data(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, fSucces, fError, "obj");
		}

		var do_send_lock_end = function(data) {
			var ref 		= {};
			ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_LCK_END);
			ref["lock_id"]	= pr_lock.id;
			
			var fSucces		= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_MOD])); 
			fSucces.push(req_gl_funct(null	, do_show_Obj						, [pr_ctr_Main.var_lc_MODE_SEL]));
			fSucces.push(req_gl_funct(null	, do_refresh_list					, [data.data]));

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			data.do_lc_send_data(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, fSucces, fError, "obj");
		}

		var do_send_duplicate_category = function(data) {
			var ref				= {};
			ref 				= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_DUPLICATE);

			var sendableData	= data.req_sendable_data(ref, "obj");
			var fSucces			= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_NEW])); 

			var fError 			= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, sendableData, 100000, fSucces, fError) ;
		}

		//-------------------------------------------------------------------------------------------------------------		
		//-------------------------------------------------------------------------------------------------------------		
		this.do_lc_Lock_Begin = function (obj){
			var ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_LCK_NEW);		

			var lock 			= {};			
			lock.objectType 	= pr_TYP_PARENT_CAT_MARKET; 	//integer
			lock.objectKey 		= obj.id; 		//integer
			ref['req_data'	]	= JSON.stringify(lock); 

			var fSucces			= [];
			fSucces.push(req_gl_funct(null, do_begin_lock, [obj]));

			var fError 			= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;	
		}


		this.do_lc_Lock_Cancel = function (obj){
			if (!pr_lock){
				App.data.mode = pr_ctr_Main.var_lc_MODE_INIT;
				do_lc_show ({}, App.data.mode);
				return;
			}
			
			var ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_LCK_DEL);		

			ref['lock_id'	]	= pr_lock.id;
			var fSucces			= [];
			fSucces.push(req_gl_funct(null, do_del_lock, []									));	
			fSucces.push(req_gl_funct(null, do_show_Obj, [pr_ctr_Main.var_lc_MODE_SEL, obj]	));	

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;	
		}	


		//---------private lock-----------------------------------------------------------------------------
		function do_begin_lock(sharedJson, obj){
			//to comeback on tab curent active
			do_gl_req_tab_active($(pr_divContent));

			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {					
				pr_lock 		= sharedJson[App['const'].RES_DATA];   
				App.data.mode 	= pr_ctr_Main.var_lc_MODE_MOD;				

				pr_ctr_EntBtn.do_lc_show(obj, App.data.mode);
				self.do_lc_show			(obj, App.data.mode);
//				do_Enabled_Edit();
			} else if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_NO) {
				pr_lock 	= null;
				var uName 	= sharedJson[App['const'].RES_DATA].username;
				do_gl_show_Notify_Msg_Error ($.i18n('lock_err_begin') + uName);
			}else{
				pr_lock = null;
				do_gl_show_Notify_Msg_Error ($.i18n('lock_err_inconnu'));
				//notify something if the lock is taken by other person
				//show lock.information
			}		
		}

		function do_del_lock(sharedJson, obj){
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {					
				pr_lock = null;				
			} else {   
				pr_lock = null;
				//notify something
				do_gl_show_Notify_Msg_Error ($.i18n('lock_err_inconnu') );
			}

			App.data.mode 	= pr_ctr_Main.var_lc_MODE_SEL;				

			pr_ctr_EntBtn.do_lc_show(obj, App.data.mode);
			self.do_lc_show			(obj, App.data.mode);	

		}

		this.can_lc_have_lock = function (){
			if (this.pr_lock!=null)
				App.MsgboxController.do_lc_show({
					title	: $.i18n('lock_err_title') ,
					content	: $.i18n('lock_err_msg')
				});	
			return this.pr_lock!=null;
		}

		var do_refresh_list = function(sharedJSon, obj) {
			if(sharedJSon[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
				pr_ctr_List.do_lc_show_byType(obj.typ00);
			}
		}

		var do_Enabled_Edit = function(){
			$("#div_Ent").find("input, select, textarea").removeAttr("disabled");
			//$("#div_TpyCat_Ent").find(".setDisabled").prop("disabled", true);
		}

		function do_show_success_msg(sharedJson, msg){
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) do_gl_show_Notify_Msg_Success (msg);
		}
		
		var do_init_detail_table = function(obj, mode){
			
			if (obj && obj.descr && typeof obj.descr === 'string'){
				obj.descr = JSON.parse(obj.descr);
			} 
			
			var additionalConfig = {
					"lang": {
						/*fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {		
						
						if (oData.lang != null) {
							$(nTd).html(oData.lang);
						}
							
						var lstLang = [{ "id": 1, "lang" :"ch"}, { "id": 2, "lang" :"en"}, { "id": 3, "lang" :"fr"}, {"id": 4, "lang" :"kr"}, { "id": 5, "lang" :"jp"}, { "id": 6, "lang" :"vn"} ];
						//var lstLang = ["ch", "en", "fr", "kr", "jp", "vn"];
			
						$(nTd).off("focusin");
							$(nTd).on("focusin", function () {
								do_gl_set_input_autocomplete(nTd, {
									dataTab : {"lang": "lang"},
									source: lstLang,
									minLength: 0
								}, oData);
							})
						},*/
					},
			}

			req_gl_create_datatable(obj, "#table_descr", additionalConfig, pr_default_new_line, function(){
				if(mode == pr_ctr_Main.var_lc_MODE_MOD || mode == pr_ctr_Main.var_lc_MODE_NEW) {
					do_gl_enable_edit($(pr_divContent));
				}
			});
		}.bind(this); 
	}
	return TpyCategoryEnt;
});