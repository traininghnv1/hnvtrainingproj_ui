define([
        'jquery',
        
        'text!group/aut/right/tmpl/AutRight_Main.html',
        
        'group/aut/right/ctrl/AutRightList',
        'group/aut/right/ctrl/AutRightEnt',
       
        ],
        function($,         		
        		AutRight_Main,
        		
        		AutRightList, 
        		AutRightEnt
        	
        ) {

	var AutRightMain 	= function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;
		
		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;		
		//------------------------------------------------------------------------------------
		
		this.var_lc_MODE_INIT 		= 0;
		this.var_lc_MODE_NEW 		= 1; //duplicate is the mode new after clone object
		this.var_lc_MODE_MOD 		= 2;
		this.var_lc_MODE_DEL 		= 3;	
		this.var_lc_MODE_SEL 		= 5;
	
		this.var_lc_URL_Aut_Header	= null;
		//---------------------------------------------------------------
		const pr_TYPE_LEVEL_01      = 1;
		const pr_TYPE_LEVEL_02      = 2;
		const pr_TYPE_LEVEL_03      = 3;
		const pr_TYPE_LEVEL_04      = 4;
		
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			self.var_lc_URL_Aut_Header				= req_gl_Security_HttpHeader_Bearer (App.keys.KEY_STORAGE_CREDENTIAL);
			do_gl_refresh_SecuHeader (self);
			
			//---------------------------------------------------------------

			tmplName.AUT_RIGHT_MAIN								= "AutRight_Main";
			tmplContr.do_lc_put_tmpl(tmplName.AUT_RIGHT_MAIN	, AutRight_Main); 
			//---------------------------------------------------------------
			if (!App.controller.AutRight)				
				 App.controller.AutRight				= {};
			
			if (!App.controller.AutRight.Main		)	
				App.controller.AutRight.Main 			= this; //important for other controller can get ref, when new this controller,
			
			if (!App.controller.AutRight.List		)  
				 App.controller.AutRight.List			= new AutRightList				("#div_List", "#div_List_Header", "#div_List_Content");				
			if (!App.controller.AutRight.Ent		)  
				 App.controller.AutRight.Ent			= new AutRightEnt				(null, "#div_Ent", null);
			

			App.controller.AutRight.List		.do_lc_init();
			App.controller.AutRight.Ent			.do_lc_init();
			//--------------------------------------------------------------------------------------------------
		}
		
		
		this.do_lc_show = function(sharedJson){
			try { 
				$(pr_divContent)		.html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_RIGHT_MAIN, {}));	
//				do_gl_apply_right($(pr_divContent));
				
//				
				App.controller.AutRight.List.do_lc_show();
				App.controller.AutRight.Ent	.do_lc_show(null);//init: obj is null	
							
			}catch(e) {				
				do_gl_send_exception(App.path.BASE_URL_API, self.var_lc_URL_Aut_Header, App.network, "Mcq.Group", "AutRightMain", "do_lc_show", e.toString()) ;
			}
//			do_gl_enhance_within($(pr_divContent));
		};
		
		this.do_lc_binding_pages = function(div, options) {
			try {
				if(div.length>0) do_gl_enhance_within(div, options);
			} catch (e) {
				console.log(e);
				self.do_show_Msg(null, e);
			}
		};
		
		//--------------------------------------------------------------------------------------------
		this.do_show_Notify_Msg  = function (sharedJson, msg, typeNotify, modeOld, modeNew){
			if (typeNotify){				
				if (typeNotify == 1 ){
					if (!msg) msg = "OK";
					do_gl_show_Notify_Msg_Success 	(msg);
				} else if (typeNotify == 0){
					if (!msg) msg = "Err";
					do_gl_show_Notify_Msg_Error 	(msg);
				}
			}else{
				var code 		= sharedJson[App['const'].SV_CODE];
				var pr_ctr_Main	= App.controller.AutRight.Main;
				if(code == App['const'].SV_CODE_API_YES) {
					
					if (modeOld == pr_ctr_Main.var_lc_MODE_NEW || modeOld == pr_ctr_Main.var_lc_MODE_MOD){
						if (!msg){
							msg = $.i18n('common_ok_msg_save');
						}
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_DEL){
						if (!msg) msg = $.i18n('common_ok_msg_del');		  
					}
					
					do_gl_show_Notify_Msg_Success 	(msg);
					
				}else if (code== App['const'].SV_CODE_API_NO){
					if (modeOld == pr_ctr_Main.var_lc_MODE_INIT || modeOld == pr_ctr_Main.var_lc_MODE_SEL){
						if (!msg) msg = $.i18n('common_err_msg_get');		        		
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_NEW || modeOld == pr_ctr_Main.var_lc_MODE_MOD){
						if (!msg) msg = $.i18n('common_err_msg_save');		        		
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_DEL){
						if (!msg) msg = $.i18n('common_err_msg_del');		  
					}
					
					do_gl_show_Notify_Msg_Error 	(msg);
					
	        	} else {
	        		if (!msg) msg = $.i18n('common_err_msg_unknow');
	        		do_gl_show_Notify_Msg_Error 	(msg);
	        	}	
			}			
		}

		//--------------------------------------------------------------------------------------------
		this.do_show_Msg= function(sharedJson, msg){
			//alert(msg);
			console.log("do_show_Msg::" + msg);
		}		
			
	};

	return AutRightMain;
  });