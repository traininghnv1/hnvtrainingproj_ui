define([
	'jquery',
	'text!group/aut/right/tmpl/AutRight_Ent.html',

	 'group/aut/right/ctrl/AutRightEntHeader',
     'group/aut/right/ctrl/AutRightEntBtn' 
	],
	function($, 
			AutRight_Ent,
	
			AutRightEntHeader, 
    		AutRightEntBtn
	) {


	var AutRightEnt     = function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;

		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;

		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 						= this;		
		//------------------------------------------------------------------------------------
		var pr_OBJ_TYPE= 1003;// change to adapt with back office for lock tool

		var pr_SERVICE_CLASS		= "ServiceAutRight"; //to change by your need

		var pr_SV_GET				= "SVGet"; 
		var pr_SV_NEW				= "SVNew"; 
		var pr_SV_DEL				= "SVDel"; 

//		var pr_SV_MOD				= "SVMod"; 	//if not use lock

		var pr_SV_LCK_NEW			= "SVLckReq"; 
		var pr_SV_LCK_SAV			= "SVLckSav";
		var pr_SV_LCK_END			= "SVLckEnd"; 
		var pr_SV_LCK_DEL			= "SVLckDel"; 

		var pr_SV_DUPLICATE			= "SVDuplicate"; 
		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntHeader 		= null;
		var pr_ctr_EntBtn 			= null;
		var pr_ctr_EntTabs 			= null;


		//-----------------------------------------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		var pr_lock					= null;
		
		var pr_default_new_line	= {
				id 			: null,
				name		: null,
				lang		: null,	
		};
		
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			tmplName.AUT_RIGHT_ENT							= "AutRight_Ent";
			tmplContr.do_lc_put_tmpl(tmplName.AUT_RIGHT_ENT	, AutRight_Ent); 		
			
			if (!App.controller.AutRight.EntBtn		)  
				 App.controller.AutRight.EntBtn			= new AutRightEntBtn			(null, "#div_Ent_Btn", null);
			if (!App.controller.AutRight.EntHeader	)  
				 App.controller.AutRight.EntHeader		= new AutRightEntHeader			(null, "#div_Ent_Header", null);
			
			pr_ctr_Main 			= App.controller.AutRight.Main;
			pr_ctr_List 			= App.controller.AutRight.List;
			pr_ctr_Ent				= App.controller.AutRight.Ent;
			
			pr_ctr_EntHeader 		= App.controller.AutRight.EntHeader;
			pr_ctr_EntBtn 			= App.controller.AutRight.EntBtn;


			pr_ctr_EntHeader.do_lc_init();
			pr_ctr_EntBtn	.do_lc_init();
		}

		//---------show-----------------------------------------------------------------------------
		this.do_lc_show		= function(obj, mode){			
			pr_object 	= obj;
			pr_mode		= !mode?pr_ctr_Main.var_lc_MODE_INIT: mode;

		

			if (pr_mode == pr_ctr_Main.var_lc_MODE_INIT){
				$("#div_Ent").html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_RIGHT_ENT, {mode : pr_ctr_Main.var_lc_MODE_INIT}));
				pr_ctr_EntBtn		.do_lc_show(obj, pr_ctr_Main.var_lc_MODE_INIT);
				return;
			}

			if (!obj){
				$("#div_Ent").html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_RIGHT_ENT, {mode: pr_ctr_Main.var_lc_MODE_INIT}));		
				pr_ctr_EntBtn		.do_lc_show(null, pr_ctr_Main.var_lc_MODE_INIT);
			}else{
				$("#div_Ent").html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_RIGHT_ENT, obj));				
				pr_ctr_EntHeader	.do_lc_show(obj, mode);
				pr_ctr_EntBtn		.do_lc_show(obj, mode);
			}	

			pr_ctr_Main.do_lc_binding_pages($("#div_Ent_Header"),{
				obj: obj
			});

			if(mode == pr_ctr_Main.var_lc_MODE_NEW) {

			} else if(mode == pr_ctr_Main.var_lc_MODE_MOD) {
				do_gl_enable_edit($(pr_divContent));
			} else {
				do_gl_disable_edit($(pr_divContent));
			}
			
			//-------------------------------------------------------------
			App.controller.DBoard.DBoardMain.do_lc_bind_event_div_Minimize();
			App.controller.DBoard.DBoardMain.do_lc_prevent_winClosing (mode);
		}

		//---show after ajax request---------------------------
		var do_show_Obj = function (sharedJson, mode, localObj){
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {					
				App.data.mode		= mode;
				if (localObj){
					self.do_lc_show(localObj, mode); 
				}else{
					var object = sharedJson[App['const'].RES_DATA];        		
					self.do_lc_show(object, mode);  
				}			     		
			} else if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_ERROR){
				do_gl_show_Notify_Msg_Error ($.i18n('common_err_msg_save'));
				return;
			}		
		}
		
		//----------------------------------------------------------------------------
		this.do_lc_show_ById = function(obj, mode){
			var svName 		= pr_SV_GET;
			if (pr_mode == pr_ctr_Main.var_lc_MODE_MOD){
				//not use lock
//				svName = pr_SV_GET

				//use lock
				svName = pr_SV_LCK_NEW
			}

			var ref 		= req_gl_Request_Content_Send(pr_SERVICE_CLASS, svName);			
			ref.id			= obj.id;
			ref["forced"]   = true;

			var fSucces		= [];		
			fSucces.push(req_gl_funct(null, do_show_Obj, [mode]));	

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;			
		}

		//---------------------------------------------new object-------------------------------------------
		this.do_lc_new = function() {
			var newObj		 = {};
			
			//action mode
			App.data.mode 	= pr_ctr_Main.var_lc_MODE_NEW;
			
			self		.do_lc_show(newObj, App.data.mode);
		}

		//---------------------------------------------clone object-------------------------------------------
		this.do_lc_duplicate = function (obj){
			var newObj 	= $.extend(true, {}, obj);
			newObj.id	= null;
			App.data.mode 	= pr_ctr_Main.var_lc_MODE_NEW;

			self		.do_lc_show(newObj, App.data.mode);
		}

		//---------del obj-----------------------------------------------------------------------------
		this.do_lc_delete 		= function (obj){
			var ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_DEL);

			var lock 			= {};			
			lock.objectType 	= pr_OBJ_TYPE; 	//integer
			lock.objectKey 		= obj.id; 		//integer
			ref['lock'	]		= JSON.stringify(lock);
			ref["id"]			= obj.id;
			
			var fSucces			= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_DEL])); 
			fSucces.push(req_gl_funct(null	, do_show_Obj						, [pr_ctr_Main.var_lc_MODE_INIT]));	
			fSucces.push(req_gl_funct(null	, do_refresh_list					, [obj])); //refresh menu

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;			
		}


		//---------Lock-----------------------------------------------------------------------------
		this.do_lc_save		= function(obj, mode){	//save new object or save with lock		
			//to comeback on tab curent active
			do_gl_req_tab_active($(pr_divContent));
					
			if(!obj.files) obj.files = [];
			var	data = req_gl_data({
				dataZoneDom		: $("#div_Ent"),
				oldObject 		: {"files": obj.files},
				removeDeleted	: true				
			});
			
			//check data error
			if(data.hasError == true){
				do_gl_show_Notify_Msg_Error ($.i18n('tpy_cat_data_error_msg'));
				return false;
			}
				
			App.MsgboxController.do_lc_show({
				title	: $.i18n("msgbox_confirm_title"),
				content : $.i18n("common_msg_mod_confirm"),
				buttons	: {
					SAVE_EXIT: {
						lab		: 	$.i18n("common_btn_save_exit"),
						funct	: 	function(){
							if(mode==pr_ctr_Main.var_lc_MODE_MOD){
								do_send_mod_exit(data)
							} else if(mode==pr_ctr_Main.var_lc_MODE_NEW){
								do_send_new_exit(data);	
							}
						}							
					},
					SAVE_CONTINUE: {
						lab		:  	$.i18n("common_btn_save_continue"),
						funct	: 	function(){
							if(mode==pr_ctr_Main.var_lc_MODE_MOD){
								do_send_mod_continue(data)
							} else if(mode==pr_ctr_Main.var_lc_MODE_NEW){
								do_send_new_continue(data);	
							}
						}
					}
				}
			});	
		}

		//-------------------------------------------New-------------------------------------------------------------
		var do_send_new_continue = function(data) {
			var ref		= {};
			ref 		= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_NEW);
			ref["lock"]	= 1;
			
			var fSucces			= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg, [null, null, pr_ctr_Main.var_lc_MODE_NEW])); 
			fSucces.push(req_gl_funct(null	, do_refresh_list				, [data.data]));
			fSucces.push(req_gl_funct(null	, do_show_Obj					, [pr_ctr_Main.var_lc_MODE_MOD]));
			fSucces.push(req_gl_funct(null	, do_begin_lock_new				, []));
			
			var fError 			= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	
			
			data.do_lc_send_data(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, fSucces, fError, "obj");
		}
		
		
		var do_send_new_exit = function(data) {
			var ref		= {};
			ref 		= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_NEW);
			ref["lock"]	= 0;
			
			var fSucces	= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_NEW])); 
			fSucces.push(req_gl_funct(null	, do_refresh_list					, [data.data]));
			fSucces.push(req_gl_funct(null	, do_show_Obj						, [pr_ctr_Main.var_lc_MODE_SEL]));
			
			var fError 	= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			data.do_lc_send_data(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, fSucces, fError, "obj");
		}
		
		//-------------------------------------------Mod-------------------------------------------------------------
		var do_send_mod_continue = function(data) {
			var ref 		= {};
			ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_LCK_SAV);
			ref["lock_id"]	= pr_lock.id;
			
			var fSucces	= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_MOD])); 
			fSucces.push(req_gl_funct(null	, do_show_Obj						, [pr_ctr_Main.var_lc_MODE_MOD]));
			fSucces.push(req_gl_funct(null	, do_refresh_list					, [data.data]));


			var fError 	= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			data.do_lc_send_data(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, fSucces, fError, "obj");
		}
		
		var do_send_mod_exit = function(data) {
			var ref 		= {};
			ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_LCK_END);
			ref["lock_id"]	= pr_lock.id;
			
			var fSucces		= [];
			fSucces.push(req_gl_funct(App	, pr_ctr_Main.do_show_Notify_Msg	, [null, null, pr_ctr_Main.var_lc_MODE_MOD])); 
			fSucces.push(req_gl_funct(null	, do_show_Obj						, [pr_ctr_Main.var_lc_MODE_SEL]));
			fSucces.push(req_gl_funct(null	, do_refresh_list					, [data.data]));

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			data.do_lc_send_data(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, fSucces, fError, "obj");
		}


		//-------------------------------------------------------------------------------------------------------------		
		//-------------------------------------------------------------------------------------------------------------		
		this.do_lc_Lock_Begin = function (obj){
			var ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_LCK_NEW);		

			var lock 			= {};			
			lock.objectType 	= pr_OBJ_TYPE; 	//integer
			lock.objectKey 		= obj.id; 		//integer
			ref['req_data'	]	= JSON.stringify(lock); 

			var fSucces			= [];
			fSucces.push(req_gl_funct(null, do_begin_lock, [obj]));

			var fError 			= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;	
		}


		this.do_lc_Lock_Cancel = function (obj){
			if (!pr_lock){
				App.data.mode = pr_ctr_Main.var_lc_MODE_INIT;
				do_lc_show ({}, App.data.mode);
				return;
			}
			
			var ref 			= req_gl_Request_Content_Send(pr_SERVICE_CLASS, pr_SV_LCK_DEL);		

			ref['lock_id'	]	= pr_lock.id;
			var fSucces			= [];
			fSucces.push(req_gl_funct(null, do_del_lock, []									));	
			fSucces.push(req_gl_funct(null, do_show_Obj, [pr_ctr_Main.var_lc_MODE_SEL, obj]	));	

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Notify_Msg, [$.i18n("common_err_ajax"), 0]);	

			App.network.do_lc_ajax (App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;	
		}	


		//---------private lock-----------------------------------------------------------------------------
		var do_begin_lock_new = function(sharedJson, obj){
			pr_lock 	= null;
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {					
				pr_lock 		= sharedJson[App['const'].RES_LOCK];   
				App.data.mode 	= pr_ctr_Main.var_lc_MODE_MOD;		
				do_gl_req_tab_active($(pr_divContent));
			} else{
				do_gl_show_Notify_Msg_Error ($.i18n('lock_err_inconnu'));
			}
		}
		
		function do_begin_lock(sharedJson, obj){
			//to comeback on tab curent active
			do_gl_req_tab_active($(pr_divContent));

			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {					
				pr_lock 		= sharedJson[App['const'].RES_DATA];   
				App.data.mode 	= pr_ctr_Main.var_lc_MODE_MOD;				

				pr_ctr_EntBtn.do_lc_show(obj, App.data.mode);
				self.do_lc_show			(obj, App.data.mode);
			} else if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_NO) {
				pr_lock 	= null;
				var uName 	= sharedJson[App['const'].RES_DATA].username;
				do_gl_show_Notify_Msg_Error ($.i18n('lock_err_begin') + uName);
			}else{
				pr_lock = null;
				do_gl_show_Notify_Msg_Error ($.i18n('lock_err_inconnu'));
				//notify something if the lock is taken by other person
				//show lock.information
			}		
		}

		function do_del_lock(sharedJson, obj){
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {					
				pr_lock = null;				
			} else {   
				pr_lock = null;
				//notify something
				do_gl_show_Notify_Msg_Error ($.i18n('lock_err_inconnu') );
			}

			App.data.mode 	= pr_ctr_Main.var_lc_MODE_SEL;				

			pr_ctr_EntBtn.do_lc_show(obj, App.data.mode);
			self.do_lc_show			(obj, App.data.mode);	

		}

		this.can_lc_have_lock = function (){
			if (this.pr_lock!=null)
				App.MsgboxController.do_lc_show({
					title	: $.i18n('lock_err_title') ,
					content	: $.i18n('lock_err_msg')
				});	
			return this.pr_lock!=null;
		}

		var do_refresh_list = function(sharedJSon, obj) {
			if(sharedJSon[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
				pr_ctr_List.do_lc_show();
			}
		}

		function do_show_success_msg(sharedJson, msg){
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) do_gl_show_Notify_Msg_Success (msg);
		}
	}
	return AutRightEnt;
});