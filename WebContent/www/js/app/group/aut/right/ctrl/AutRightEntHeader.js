define([
	'jquery',
	'text!group/aut/right/tmpl/AutRight_Ent_Header.html',
	],
	
	function($, 
			AutRight_Ent_Header
	) {


	var AutRightEntHeader     = function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;

		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;

		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		//------------------------------------------------------------------------------------

		var svClass         			= App['const'].SV_CLASS;
		var svName          			= App['const'].SV_NAME;
		var userId          			= App['const'].USER_ID;
		var sessId          			= App['const'].SESS_ID;
		var fVar            			= App['const'].FUNCT_SCOPE;
		var fName           			= App['const'].FUNCT_NAME;
		var fParam          			= App['const'].FUNCT_PARAM;

		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;

		//-----------------------------------------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;

		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			pr_ctr_Main 			= App.controller.AutRight.Main;
			pr_ctr_List 			= App.controller.AutRight.List;
			pr_ctr_Ent				= App.controller.AutRight.Ent;
			
			tmplName.AUT_RIGHT_ENT_HEADER						= "AutRight_EntHeader";
			tmplContr.do_lc_put_tmpl(tmplName.AUT_RIGHT_ENT_HEADER	, AutRight_Ent_Header); 		

		}      

		this.do_lc_show		= function(obj, mode){
			pr_object 	= obj;
			pr_mode		= mode;

			try{
				$(pr_divContent)				.html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_RIGHT_ENT_HEADER , obj));
				
				if(obj != null){
					$("#sel_grp")	.find("option[value="+obj.grp+"]")  	.attr("selected","selected");
				} 
				
				do_bind_event(obj, mode);

			}catch(e) {				
				do_gl_send_exception(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Header, App.network, "mcq.group", "AutRightEntHeader", "do_lc_show", e.toString()) ;
			}
		};

		var do_bind_event =  function (obj, mode){
		}
		
	};


	return AutRightEntHeader;
});