define([
        'jquery',
        'text!group/aut/right/tmpl/AutRight_List.html',
        'text!group/aut/right/tmpl/AutRight_List_Table.html',
        
        ],
        function($, 
        		AutRight_List,
        		AutRight_List_Table
        ) 
        {

	var AutRightList 	= function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;
		
		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		
		//------------------------------------------------------------------------------------
		var pr_SERVICE_CLASS		= "ServiceAutRight";
		
		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		//-----------------------------------------------------------------------------------
		//--------------------APIs--------------------------------------//
		this.do_lc_init				= function(){
			pr_ctr_Main 			= App.controller.AutRight.Main;
			pr_ctr_List 			= App.controller.AutRight.List;
			pr_ctr_Ent				= App.controller.AutRight.Ent;
			
			//-----------------------------------------------------------------------------------
			tmplName.AUT_RIGHT_LIST								= "AutRight_List";
			tmplName.AUT_RIGHT_LIST_TABLE						= "AutRight_List_Table";
			
			tmplContr.do_lc_put_tmpl(tmplName.AUT_RIGHT_LIST		, AutRight_List); 
			tmplContr.do_lc_put_tmpl(tmplName.AUT_RIGHT_LIST_TABLE	, AutRight_List_Table); 
			
		}
		//---------show-----------------------------------------------------------------------------
		this.do_lc_show = function(){               
			try{
				$("#div_List")			.html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_RIGHT_LIST			, {}));
				$("#div_List_Table") 	.html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_RIGHT_LIST_TABLE	, {}));
				do_get_list_ByAjax_Dyn("#div_List_Table");
				
				//---------------------------------------------------------------
				App.controller.DBoard.DBoardMain.do_lc_bind_event_div_Minimize();
			}catch(e) {	
				console.log(e);
				do_gl_send_exception(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Header, App.network, "group.aut", "AutRightList", "do_lc_show", e.toString()) ;
			}
		};
		
		//--------------------------------------------------------------------------------------------
		var do_get_list_ByAjax_Dyn 	= function(div){	
			var ref 				= req_gl_Request_Content_Send(pr_SERVICE_CLASS, "SVLstDyn");
			
			var lang = localStorage.language;
			if (lang ==null ) lang 	= "en";	
			var filename 			= "www/js/lib/datatables/datatable_"+lang+".json";
			
			var additionalConfig 	= {	
					"grp": {
						fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {		
							$(nTd).html($.i18n('aut_right_grp_'+ oData.grp));
						}
					}
			};
			
			
			var colConfig			= req_gl_table_col_config($(div).find("table"), null, additionalConfig);
			var dataTableOption 	= {
					"canScrollY"			: true,
					"searchOption" 			: false,	
//					"searchOptionConfig" 	: [	{lab: $.i18n('common_search_any')			, val:0}]
			};	     		
			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Msg, [$.i18n("common_err_ajax")]);	
			
			//call Datatable AJAX dynamic function from DatatableTool
			var oTable 		= req_gl_Datatable_Ajax_Dyn		(div, App.path.BASE_URL_API,pr_ctr_Main.var_lc_URL_Aut_Header, filename, colConfig, ref, fError, undefined, null, undefined, do_bind_list_event, dataTableOption);
		}
		
		//--------------------------------------------------------------------------------------------
		var do_bind_list_event = function(sharedJson, div, oTable) {
			do_gl_enhance_within($(div), {div: div});
			
			$(div).find('tr').off('click')
			$(div).find('tr').on('click', function(){
				if(App.data.mode == pr_ctr_Main.var_lc_MODE_MOD || App.data.mode == pr_ctr_Main.var_lc_MODE_NEW){
					do_gl_show_Notify_Msg_Error ($.i18n('common_err_msg_sel'));
					return;
				}
				//apply CSS style
				do_gl_Add_Class_List($(this).parent(), $(this), "selected");
				
				var oData = oTable.fnGetData(this);
				pr_ctr_Ent. do_lc_show_ById(oData, pr_ctr_Main.var_lc_MODE_SEL);
		
			});
		};
	
	};

	return AutRightList;
  });