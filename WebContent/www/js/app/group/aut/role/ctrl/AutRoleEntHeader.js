define([
        'jquery',
        'text!group/aut/role/tmpl/Aut_Role_Ent_Header.html'

        ],

        function($, 
        		AutRole_Ent_Header) {


	var AutRoleEntHeader     = function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;

		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		//------------------------------------------------------------------------------------

		var svClass         			= App['const'].SV_CLASS;
		var svName          			= App['const'].SV_NAME;
		var userId          			= App['const'].USER_ID;
		var sessId          			= App['const'].SESS_ID;
		var fVar            			= App['const'].FUNCT_SCOPE;
		var fName           			= App['const'].FUNCT_NAME;
		var fParam          			= App['const'].FUNCT_PARAM;

		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntHeader 		= null;
		var pr_ctr_EntBtn 			= null;
		var pr_ctr_EntTabs 			= null;
		
	
		//-----------------------------------------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			//----step 01: load template----------------------------------------------------------------------------------------------
			tmplName.AUT_ROLE_ENT_HEADER	= "Aut_Role_Ent_Header";
			tmplContr.do_lc_put_tmpl(tmplName.AUT_ROLE_ENT_HEADER	, AutRole_Ent_Header); 		
			
			//-----------------------------------------------------------------------------------
			pr_ctr_Main 			= App.controller.AutRole.Main;
			pr_ctr_List 			= App.controller.AutRole.List;
			pr_ctr_Ent				= App.controller.AutRole.Ent;
		}      
		
		this.do_lc_show		= function(obj, mode){
			pr_object 	= obj;
			pr_mode		= mode;
			
			try{
					
				$(pr_divContent).html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_ROLE_ENT_HEADER, obj));

				//fixed max-height scroll of % height div_ContentView
				//do_gl_calculateScrollBody(pr_divContent + " .custom-scroll-header", 25);
				
				do_bind_event(obj, mode);
			}catch(e) {				
				do_gl_send_exception(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Header, App.network, "aut.role", "AutRoleEntHeader", "do_lc_show", e.toString()) ;
			}
		};
			
		//---------private-----------------------------------------------------------------------------
		var do_bind_event = function (obj, mode){
			var rightSocMa = pr_ctr_Main.do_verify_user_right_soc_manage();
			if(!rightSocMa)
				$("#div_aut_role_societe").hide();
			else{
				var LstAllSociete = App.data["LstSociete"].concat(App.data["LstSocieteChild"]);
				for(var i=0; i<LstAllSociete.length; i++){
					if(LstAllSociete[i].id == obj.manId){
						$("#inp_aut_role_societe")	.val(LstAllSociete[i].name01);
						break;
					}
				}
				do_gl_autocomplete({
					el: $("#inp_aut_role_societe"),
					required: true,
					source: App.data["LstSociete"].concat(App.data["LstSocieteChild"]),
					selectCallback: function(item ) {
						$("#socId")					.val(item.id);
						$("#inp_aut_role_societe")	.val(item.name01);
					},
					renderAttrLst: ["name01"],
					minLength: 0,
				});
			}
		}

	};


	return AutRoleEntHeader;
});