define([
        'jquery',
        'text!group/aut/role/tmpl/Aut_Role_Ent_Tab_Rights.html'      

        ],
        function($,
        		AutRole_Ent_Tab_Rights    		
        		) {


	var AutRoleEntTabRights     = function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;

		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		//------------------------------------------------------------------------------------
		
		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntHeader 		= null;
		var pr_ctr_EntBtn 			= null;
		var pr_ctr_EntTabs 			= null;
		
	
		//-----------------------------------------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			//----step 01: load template----------------------------------------------------------------------------------------------
			tmplName.AUT_ROLE_ENT_TAB_RIGHT	= "Aut_Role_Ent_Tab_Right";
			tmplContr.do_lc_put_tmpl(tmplName.AUT_ROLE_ENT_TAB_RIGHT, AutRole_Ent_Tab_Rights);
			//-----------------------------------------------------------------------------------
			
			pr_ctr_Main 			= App.controller.AutRole.Main;
			pr_ctr_List 			= App.controller.AutRole.List;
			pr_ctr_Ent				= App.controller.AutRole.Ent;
			
		}
		
		this.do_lc_show		= function(obj, mode){
			pr_object 	= obj;
			pr_mode		= mode;
			
			try {
				var rightList = do_check_selected_right(obj.rights);
				
				$(pr_divContent).html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_ROLE_ENT_TAB_RIGHT, rightList));
				
				do_bind_event (obj, mode, rightList);
				
			} catch(e) {		
				console.log(e);
			}
		};

		//---------private-----------------------------------------------------------------------------
		var do_bind_event = function (obj, mode, array){
			$.each(array, function (key, val){
				var div = "#div_Tab_" + key + " input[type='checkbox']";
				$(div+"[data-level='0']").off("click");
				$(div+"[data-level='0']").click(function() {
					var checked = $(this).prop('checked');
					$(div+"[data-level='1']").prop('checked', checked);
				});
			});
	
		}
		
		var do_check_selected_right = function(rights) {
			var tabCheck = {};
			for (var i in rights) tabCheck[rights[i]] = 1;
			
			var grpRight = $.extend(true, {}, App.data.AutRightList);
			for (var grp in grpRight){
				var grpList = grpRight[grp];
				grpList.checkAll = true;
				
				for (var r in grpList.rights){
					var ri = grpList.rights[r];
					if (tabCheck[ri.id]) 
							ri.check 		 = true; 
					else 	grpList.checkAll = false;
				}
			}
			
			//----sort by grp name
			var key = Object.keys(grpRight);
			key.sort();
			var arr = [];
			for (var k in key){
				arr.push (grpRight[key[k]]);
			}
			
			return arr;
		}
	};


	return AutRoleEntTabRights;
});

