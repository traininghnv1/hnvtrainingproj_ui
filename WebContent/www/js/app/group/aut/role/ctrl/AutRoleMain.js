define([
        'jquery',
        
        'text!group/aut/role/tmpl/Aut_Role_Main.html',
        
        'group/aut/role/ctrl/AutRoleList',
        'group/aut/role/ctrl/AutRoleEnt',
         
       
      
        ],
        function($,         		
        		AutRole_Main, 
        		
        		AutRoleList, 
        		AutRoleEnt 
        		
        ) {

	var AutRoleMain 	= function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;
		
		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;		
		//------------------------------------------------------------------------------------
		
		this.var_lc_MODE_INIT 		= 0;
		this.var_lc_MODE_NEW 		= 1; //duplicate is the mode new after clone object
		this.var_lc_MODE_MOD 		= 2;
		this.var_lc_MODE_DEL 		= 3;	
		this.var_lc_MODE_SEL 		= 5;
	
		this.var_lc_URL_Aut_Header	= null;
		
		//-----------------------------------------------------------------------------------
		this.pr_right_soc_manage	= [30002001, 30002002, 30002003, 30002004, 30002005];
		//---------------------------------------------------------------
		//---------------------------------------------------------------
		
					
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			//----step 01: load template---------------------------------------------------------------------------------------
			tmplName.AUT_ROLE_MAIN			= "Aut_Role_Main";
			tmplContr.do_lc_put_tmpl(tmplName.AUT_ROLE_MAIN	, AutRole_Main); 
			
			//----step 02: load controller------------------------------------------------------------------------------------
			if (!App.controller.AutRole)				
				 App.controller.AutRole				= {};
			if (!App.controller.AutRole.Main)	
				App.controller.AutRole.Main 		= this; //important for other controller can get ref, when new this controller,
			if (!App.controller.AutRole.List		)  
				 App.controller.AutRole.List		= new AutRoleList			(null, "#div_List" , null);				
			if (!App.controller.AutRole.Ent			)  
				 App.controller.AutRole.Ent			= new AutRoleEnt			(null, "#div_Ent" , null);
			
			pr_ctr_Main 							= App.controller.AutRole.Main;
			pr_ctr_List 							= App.controller.AutRole.List;
			pr_ctr_Ent								= App.controller.AutRole.Ent;
			
			pr_ctr_List								.do_lc_init();
			pr_ctr_Ent								.do_lc_init();
			
			//----step 03: define bearer header--------------------------------------------------------------------
			self.var_lc_URL_Aut_Header				= req_gl_Security_HttpHeader_Bearer (App.keys.KEY_STORAGE_CREDENTIAL);
			do_gl_refresh_SecuHeader (self);
			
			//--------------------------------------------------------------------------------------------------
						
		}
		
		
		this.do_lc_show = function(){
			try { 
				do_get_list_rights();
				
				
				$(pr_divContent).html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_ROLE_MAIN, {}));	
				do_apply_right($(pr_divContent));
				
				App.controller.AutRole.List	.do_lc_show();
				App.controller.AutRole.Ent	.do_lc_show(undefined, this.var_lc_MODE_INIT);	//init: obj is null	
						
				do_gl_init_Resizable("#div_List");	
			} catch(e) {
				console.log(e);
				do_gl_send_exception(App.path.BASE_URL_API, self.var_lc_URL_Aut_Header, App.network, "aut.role", "AutRoleMain", "do_lc_show", e.toString()) ;
			}
		};
		
		this.do_lc_binding_pages = function(div) {
			try {
				if(div.length>0) do_gl_enhance_within(div);
			} catch (e) {
				self.do_show_Msg(null, e);
			}
		};
		
		//---------private-----------------------------------------------------------------------------
		var do_get_list_rights= function(){
			//ajax to get all fix values here			
			var ref 		= req_gl_Request_Content_Send('ServiceAutRight', 'SVLst');			
		
			var fSucces		= [];		
			fSucces.push(req_gl_funct(null, do_get_list_rights_response, []));	
			
			var fError 		= req_gl_funct(App, self.do_show_Msg, [$.i18n("common_err_ajax")]);	
			App.network.do_lc_ajax (App.path.BASE_URL_API, self.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;	
		}
		
		var do_get_list_rights_response = function(sharedJson) {
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
				var data = sharedJson[App['const'].RES_DATA];
				var cache = {};
				$.each(data, function(i, e) {
					if(!cache[e.grp]) {
						cache[e.grp] = {};
						cache[e.grp].label = 'aut_right_grp_'+e.grp;
						cache[e.grp].rights = [];
					}
					cache[e.grp].rights.push(e);
				});
				App.data.AutRightList = cache;
			}
		}
		
		//--------------------------------------------------------------------------------------------
		this.do_show_Notify_Msg  = function (sharedJson, msg, typeNotify, modeOld, modeNew){
			if (typeNotify){				
				if (typeNotify == 1 ){
					if (!msg) msg = "OK";
					do_gl_show_Notify_Msg_Success 	(msg);
				} else if (typeNotify == 0){
					if (!msg) msg = "Err";
					do_gl_show_Notify_Msg_Error 	(msg);
				}
			}else{
				var code 		= sharedJson[App['const'].SV_CODE];
				var pr_ctr_Main	= App.controller.AutRole.Main;
				if(code == App['const'].SV_CODE_API_YES) {
					
					if (modeOld == pr_ctr_Main.var_lc_MODE_NEW || modeOld == pr_ctr_Main.var_lc_MODE_MOD){
						if (!msg){
							msg = $.i18n('common_ok_msg_save');
						}
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_DEL){
						if (!msg) msg = $.i18n('common_ok_msg_del');		  
					}
					
					do_gl_show_Notify_Msg_Success 	(msg);
					
				}else if (code== App['const'].SV_CODE_API_NO){
					if (modeOld == pr_ctr_Main.var_lc_MODE_INIT || modeOld == pr_ctr_Main.var_lc_MODE_SEL){
						if (!msg) msg = $.i18n('common_err_msg_get');		        		
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_NEW || modeOld == pr_ctr_Main.var_lc_MODE_MOD){
						if (!msg) msg = $.i18n('common_err_msg_save');		        		
					}else if (modeOld == pr_ctr_Main.var_lc_MODE_DEL){
						if (!msg) msg = $.i18n('common_err_msg_del');		  
					}
					
					do_gl_show_Notify_Msg_Error 	(msg);
					
	        	} else {
	        		if (!msg) msg = $.i18n('common_err_msg_unknow');
	        		do_gl_show_Notify_Msg_Error 	(msg);
	        	}	
			}			
		}			

		//--------------------------------------------------------------------------------------------
		this.do_show_Msg= function(sharedJson, msg){
			//alert(msg);
			console.log("do_show_Msg::" + msg);
		}		
			
	};

	return AutRoleMain;
  });