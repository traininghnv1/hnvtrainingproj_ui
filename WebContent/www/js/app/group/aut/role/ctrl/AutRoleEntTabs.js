define([
        'jquery',
        'text!group/aut/role/tmpl/Aut_Role_Ent_Tabs.html',
        
        'group/aut/role/ctrl/AutRoleEntTabRights' 
        ],
        function($, 
        		AutRole_Ent_Tabs ,
        		
        		AutRoleEntTabRights
        		) {


	var AutRoleEntTabs     = function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;

		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		//------------------------------------------------------------------------------------

		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntTabRight        = null;
		//-------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			//----step 01: load template----------------------------------------------------------------------------------------------
			tmplName.AUT_ROLE_ENT_TABS		= "Aut_Role_Ent_Tabs";
			tmplContr.do_lc_put_tmpl(tmplName.AUT_ROLE_ENT_TABS, AutRole_Ent_Tabs); 			
			
			//------------------------------------------------------------------------------------
			if (!App.controller.AutRole.EntTabRight)  
				 App.controller.AutRole.EntTabRight	= new AutRoleEntTabRights	(null, "#div_Ent_Tab_Rights", null);
			App.controller.AutRole.EntTabRight	.do_lc_init();
			
			//------------------------------------------------------------------------------------
			pr_ctr_Main 			= App.controller.AutRole.Main;
			pr_ctr_List 			= App.controller.AutRole.List;
			pr_ctr_Ent				= App.controller.AutRole.Ent;
			pr_ctr_EntTabRight		= App.controller.AutRole.EntTabRight;
		}     
		
		this.do_lc_show		= function(obj, mode){
			pr_object 	= obj;
			pr_mode		= mode;
			
			try{
				
				$(pr_divContent).html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_ROLE_ENT_TABS, obj));
				
				//fixed max-height scroll of % height div_ContentView
//				do_gl_calculateScrollBody(pr_divContent + " .custom-scroll-tab", 43.6);
							
				pr_ctr_EntTabRight.do_lc_show(obj, mode);
				
				do_bind_event(obj, mode);
			}catch(e) {				
				do_gl_send_exception(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Header, App.network, "aut.role", "AutRoleEntTabs", "do_lc_show", e.toString()) ;
			}
		};
		

		//---------private-----------------------------------------------------------------------------
		var do_bind_event = function (obj, mode){
			
		}

	};


	return AutRoleEntTabs;
});