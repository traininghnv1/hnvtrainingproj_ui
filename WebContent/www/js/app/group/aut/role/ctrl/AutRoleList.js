define([
        'jquery',
        'text!group/aut/role/tmpl/Aut_Role_List.html',     
        'text!group/aut/role/tmpl/Aut_Role_List_Table.html',  

        ],
        function($, 
        		Aut_Role_List,
        		Aut_Role_List_Content        		
        ) 
        {

	var AutRoleList 	= function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;
		
		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		
		
		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntHeader 		= null;
		var pr_ctr_EntBtn 			= null;
		var pr_ctr_EntTabs 			= null;		
		
		//-----------------------------------------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		
		//-----------------------------------------------------------------------------------
		var varname					= "AutRoleList";
		
		//--------------------APIs--------------------------------------//
		this.do_lc_init						= function(){
			//----step 01: load template----------------------------------------------------------------------------------------------
			tmplName.AUT_ROLE_LIST			= "Aut_Role_List";
			tmplName.AUT_ROLE_LIST_CONTENT	= "Aut_Role_List_Content";
			
			tmplContr.do_lc_put_tmpl(tmplName.AUT_ROLE_LIST			, Aut_Role_List); 
			tmplContr.do_lc_put_tmpl(tmplName.AUT_ROLE_LIST_CONTENT	, Aut_Role_List_Content);
			
			//-----------------------------------------------------------------------------------
			pr_ctr_Main 			= App.controller.AutRole.Main;
			pr_ctr_List 			= App.controller.AutRole.List;
			pr_ctr_Ent				= App.controller.AutRole.Ent;
		}
		
		//--------------------------------------------------------------------------------------------
		this.do_lc_show = function(){               
			try{
				$(pr_divContent)	.html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_ROLE_LIST				, {}));
				$("#div_List_All")	.html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_ROLE_LIST_CONTENT		, {}));
				
				do_get_list_ByAjax_Dyn	(pr_divContent);

				//---------------------------------------------------------------
				App.controller.DBoard.DBoardMain.do_lc_bind_event_div_Minimize();
			}catch(e) {	
				console.log(e);
				do_gl_send_exception(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Header, App.network, "aut.role", "AutRoleList", "do_lc_show", e.toString()) ;
			}
		};
		
		//---------private-----------------------------------------------------------------------------		
		var do_get_list_ByAjax	= function (div, varname){	
			var ref 			= {};
			ref[svClass		] 	= "ServiceAutRole"; 
			ref[svName		]	= "SVLst";
			ref[userId]			= App.data.user.id;
			ref[sessId]			= App.data.session_id;


			var fSucces			= [];	
			fSucces.push(req_gl_funct(App	, App.funct.put		, [varname]));
			fSucces.push(req_gl_funct(null	, do_show_list		, [varname]));
			fSucces.push(req_gl_funct(null	, do_bind_list_event, []));

			var fError 		= req_gl_funct(App, pr_ctr_Main.do_show_Msg, [$.i18n("common_err_ajax")]);	
			
			App.network.ajax(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;		
		}
		
		var do_show_list = function (sharedJson, varname){
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
				var data 		= App.data[varname]; //sharedJson[App['const'].RES_DATA];
				$("#div_List_All").html(tmplContr.req_lc_compile_tmpl(tmplName.AUT_ROLE_LIST_CONTENT	, data));	
			} else {
				//do something else
			}
		}
		//--------------------------------------------------------------------------------------------
		var do_get_list_ByAjax_Dyn 	= function (div){	
			var ref 				= {};
			ref[svClass] 			= "ServiceAutRole"; 
			ref[svName]				= "SVLstDyn"; 
			ref[userId]				= App.data.user.id;
			ref[sessId]				= App.data.session_id;
			
			var lang = localStorage.language;
			if (lang ==null ) lang 	= "vn";	
			
			var filename 			= "www/js/lib/datatables/datatable_"+lang+".json";
			var additionalConfig	= {};
			var dataTableOption 	= {
					"canScrollY"			: true,
					"searchOption" 			: false,	
//					"searchOptionConfig" 	: [	{lab: $.i18n('common_search_any'), val:0}]
			};	     		
			var colConfig			= req_gl_table_col_config($(div).find("table"), null, additionalConfig);
			var fError 				= req_gl_funct(App, pr_ctr_Main.do_show_Msg, [$.i18n("common_err_ajax")]);	
			
			//call Datatable AJAX dynamic function from DatatableTool
			var oTable 		= req_gl_Datatable_Ajax_Dyn		(div, App.path.BASE_URL_API,pr_ctr_Main.var_lc_URL_Aut_Header, filename, colConfig, ref, fError, undefined, null, undefined, do_bind_list_event, dataTableOption);
		}
		
		//--------------------------------------------------------------------------------------------
		var do_bind_list_event = function(sharedJson, div, oTable) {
			do_gl_enhance_within($(div), {div: div});
			
			$(div).find('tr').off('click')
			$(div).find('tr').on('click', function(){
				if(App.data.mode == pr_ctr_Main.var_lc_MODE_MOD || App.data.mode == pr_ctr_Main.var_lc_MODE_NEW){
					do_gl_show_Notify_Msg_Error ($.i18n('common_err_msg_sel'));
					return;
				}
				//apply CSS style
				do_gl_Add_Class_List($(this).parent(), $(this), "selected");
				
				var oData = oTable.fnGetData(this);
				pr_ctr_Ent. do_lc_show_ById(oData, pr_ctr_Main.var_lc_MODE_SEL);
		
			});
		};
		
		//--------------------------------------------------------------------------------------------
	
	};

	return AutRoleList;
  });