define([
        'jquery',
        
        'text!group/nso/post/tmpl/NsoPost_Main.html',
       
        'group/nso/post/ctrl/NsoPostList',
        'group/nso/post/ctrl/NsoPostEnt',
        'group/nso/post/ctrl/NsoPostEntHeader',
        'group/nso/post/ctrl/NsoPostEntBtn',
        'group/nso/post/ctrl/NsoPostEntTabs', 
//        'group/nso/post/ctrl/NsoPostEntTabRating',
        'group/nso/post/ctrl/NsoPostEntTabDoc',
        
        'group/util/html_editor/ctrl/HtmlEditorController',       
      
        ],
        function($,         		
        		NsoPost_Main, 
        		
        		NsoPostList, 
        		NsoPostEnt, 
        		NsoPostEntHeader, 
        		NsoPostEntBtn, 
        		NsoPostEntTabs, 
//        		NsoPostEntTabRating,
        		NsoPostEntTabDoc,
        		
        		HtmlEditorController
        ) {

	var NsoPostMain 	= function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;
		
		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;		
		//------------------------------------------------------------------------------------
		
		this.var_lc_MODE_INIT 		= 0;
		this.var_lc_MODE_NEW 		= 1; //duplicate is the mode new after clone object
		this.var_lc_MODE_MOD 		= 2;
		this.var_lc_MODE_DEL 		= 3;	
		this.var_lc_MODE_SEL 		= 5;
		
		this.var_lc_MODE_MOD_TRANSL	= 10;//mode translation
	
		this.var_lc_URL_Aut_Header	= null;
		
		this.var_lc_TRANSL_FILTER_ATTR_LEV_ALL 	= [
			'files'		, 'transl'		, 
			'dt01_'		, 'dt02_' 		, 
			'statLab'	, 'management'	, 'dbFlag'		, 'mode',  'DT_RowClass', 'action', 'ord', 'stat', 'typ',
			'typLab'	, 'undefined'	, 'currentPrice', 'matNb',
			"price"	];
		//---------------------------------------------------------------
		var var_lc_TYP_PARENT_CAT   = 17100;
					
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			tmplName.NSO_POST_MAIN						= "NsoPost_Main";
			tmplContr.do_lc_put_tmpl(tmplName.NSO_POST_MAIN	, NsoPost_Main); 
			
			//--------------------------------------------------------------------------------------------------
			
			if (!App.controller.NsoPost)				
				 App.controller.NsoPost				= {};
			
			if (!App.controller.NsoPost.Main)	
				App.controller.NsoPost.Main 			= this; //important for other controller can get ref, when new this controller,
			
			if (!App.controller.NsoPost.List		)  
				 App.controller.NsoPost.List		= new NsoPostList				(null, "#div_List", null);				
			if (!App.controller.NsoPost.Ent			)  
				 App.controller.NsoPost.Ent			= new NsoPostEnt				(null, "#div_Ent", null);
			if (!App.controller.NsoPost.EntBtn		)  
				 App.controller.NsoPost.EntBtn		= new NsoPostEntBtn				(null, "#div_Ent_Btn", null);
			if (!App.controller.NsoPost.EntHeader	)  
				 App.controller.NsoPost.EntHeader	= new NsoPostEntHeader			(null, "#div_Ent_Header", null);
			if (!App.controller.NsoPost.EntTabs		)  
				 App.controller.NsoPost.EntTabs		= new NsoPostEntTabs			(null, "#div_Ent_Tabs", null);
			
			//----------tab name----------------------------------------------------------------------------------------
//			if (!App.controller.NsoPost.EntTabRating)  
//				 App.controller.NsoPost.EntTabRating = new NsoPostEntTabRating	(null, "#div_Ent_Tab_Rating", null);
			if (!App.controller.NsoPost.EntTabDoc)  
				 App.controller.NsoPost.EntTabDoc	= new NsoPostEntTabDoc	(null, "#div_Ent_Tab_Doc", null);
			//--------------------------------------------------------------------------------------------------
			if (!App.controller.HtmlEditor)  
				 App.controller.HtmlEditor 			= new HtmlEditorController();
			//--------------------------------------------------------------------------------------------------
			
			self.var_lc_URL_Aut_Header				= req_gl_Security_HttpHeader_Bearer (App.keys.KEY_STORAGE_CREDENTIAL);
			do_gl_refresh_SecuHeader (self);
			//--------------------------------------------------------------------------------------------------
			
			App.controller.NsoPost.List			.do_lc_init();
			App.controller.NsoPost.Ent			.do_lc_init();
			
			App.controller.NsoPost.EntBtn		.do_lc_init();
			App.controller.NsoPost.EntHeader	.do_lc_init();
			App.controller.NsoPost.EntTabs		.do_lc_init();
//			App.controller.NsoPost.EntTabRating	.do_lc_init();
			App.controller.NsoPost.EntTabDoc	.do_lc_init();
			
			//--------------------------------------------------------------------------------------------------
		}
		
		
		this.do_lc_show = function(){
			try { 
				$(pr_divContent).html(tmplContr.req_lc_compile_tmpl(tmplName.NSO_POST_MAIN, {}));	
				
				var params = req_gl_Url_Params(App.data.url);
				if(params.id){
					var mode = params.mode;
					var lang = params.lang
					if (!mode) mode = self.var_lc_MODE_SEL;
					App.controller.NsoPost.Ent.do_lc_show_ById({id: params.id}, mode, lang);//init: obj is null
				} else {
					App.controller.NsoPost.List	.do_lc_show();
					App.controller.NsoPost.Ent.do_lc_show({}, this.var_lc_MODE_INIT);	//init: obj is null
				}

				App.controller.DBoard.DBoardMain.do_lc_bind_event_div_MaxResize	('#div_List', '#div_Ent');
				App.controller.DBoard.DBoardMain.do_lc_bind_event_div_MinResize	('#div_List', '#div_Ent');
				App.controller.DBoard.DBoardMain.do_lc_bind_event_div_Minimize	();
				
//				do_gl_init_Resizable("#div_List");		=> splitter	
			}catch(e) {				
				do_gl_send_exception(App.path.BASE_URL_API, self.var_lc_URL_Aut_Header, App.network, "nso.post", "NsoPostMain", "do_lc_show", e.toString()) ;
			}

		};
		
		this.do_lc_binding_pages = function(div, options) {
			try {
				if(div.length>0) do_gl_enhance_within(div, options);
			} catch (e) {
				self.do_show_Msg(null, e);
			}
		};

		//---------private-----------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------
			
		this.do_show_Notify_Msg  = function (sharedJson, msg, typeNotify, modeOld, modeNew){
			if (typeNotify){				
				if (typeNotify == 1 ){
					if (!msg) msg = "OK";
					do_gl_show_Notify_Msg_Success 	(msg);
				} else if (typeNotify == 0){
					if (!msg) msg = "Err";
					do_gl_show_Notify_Msg_Error 	(msg);
				}
			}else{
				var code = sharedJson[App['const'].SV_CODE];
				if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
					
					if (modeOld == self.var_lc_MODE_NEW || modeOld == self.var_lc_MODE_MOD){
						if (!msg) msg = $.i18n('common_ok_msg_save');		        		
					}else if (modeOld == self.var_lc_MODE_DEL){
						if (!msg) msg = $.i18n('common_ok_msg_del');		  
					}
					
					do_gl_show_Notify_Msg_Success 	(msg);
					
				}else if (code== App['const'].SV_CODE_API_NO){
					if (modeOld == self.var_lc_MODE_INIT || modeOld == self.var_lc_MODE_SEL){
						if (!msg) msg = $.i18n('common_err_msg_get');		        		
					}else if (modeOld == self.var_lc_MODE_NEW || modeOld == self.var_lc_MODE_MOD){
						if (!msg) msg = $.i18n('common_err_msg_save');		        		
					}else if (modeOld == self.var_lc_MODE_DEL){
						if (!msg) msg = $.i18n('common_err_msg_del');		  
					}
					
					do_gl_show_Notify_Msg_Error 	(msg);
					
	        	} else {
	        		if (!msg) msg = $.i18n('common_err_msg_unknow');
	        		do_gl_show_Notify_Msg_Error 	(msg);
	        	}	
			}			
		}	
		//-------------------------------------------
		this.do_show_Msg= function(sharedJson, msg){
			console.log("do_show_Msg::" + msg);
		}	
		//-------------------------------------------
			
	};

	return NsoPostMain;
  });