 define([
	'jquery',
	'text!group/nso/post/tmpl/NsoPost_Ent_Tab_Rating.html'
	],
	function($, 
			NsoPost_Ent_Tab_Rating) {

	var NsoPostEntTabRating     = function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;

		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;

		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		//------------------------------------------------------------------------------------

		var svClass         			= App['const'].SV_CLASS;
		var svName          			= App['const'].SV_NAME;
		var userId          			= App['const'].USER_ID;
		var sessId          			= App['const'].SESS_ID;
		var fVar            			= App['const'].FUNCT_SCOPE;
		var fName           			= App['const'].FUNCT_NAME;
		var fParam          			= App['const'].FUNCT_PARAM;

		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntHeader 		= null;
		var pr_ctr_EntBtn 			= null;
		var pr_ctr_EntTabs 			= null;


		//-----------------------------------------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			pr_ctr_Main 			= App.controller.NsoPost.Main;
			pr_ctr_List 			= App.controller.NsoPost.List;

			pr_ctr_Ent				= App.controller.NsoPost.Ent;
			pr_ctr_EntHeader 		= App.controller.NsoPost.EntHeader;
			pr_ctr_EntBtn 			= App.controller.NsoPost.EntBtn;
			pr_ctr_EntTabs 			= App.controller.NsoPost.EntTabs;

		}      

		this.do_lc_show		= function(obj, mode){
			pr_object 	= obj;
			pr_mode		= mode;

			try{
				do_Load_View();
				do_Build_Page(obj, mode);
				do_bind_event(obj, mode);
			}catch(e) {				
				do_gl_send_exception(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Header, App.network, "nso.post", "NsoPostEntTabRating", "do_lc_show", e.toString()) ;
			}
		};

		var do_Load_View = function(){
			tmplContr.do_lc_put_tmpl(tmplName.NSO_POST_ENT_TAB_RATING	, NsoPost_Ent_Tab_Rating); 
		} 

		var do_Build_Page = function(obj, mode){
			$(pr_divContent)		.html(tmplContr.req_lc_compile_tmpl(tmplName.NSO_POST_ENT_TAB_RATING, obj));
		} 

		//---------private-----------------------------------------------------------------------------
		var do_bind_event = function (obj, mode){

		}
	};

	return NsoPostEntTabRating;
});