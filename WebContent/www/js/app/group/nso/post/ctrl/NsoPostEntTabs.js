define([
        'jquery',
        'text!group/nso/post/tmpl/NsoPost_Ent_Tabs.html'

        ],
        function($, 
        		NsoPost_Ent_Tabs        		
        		) {


	var NsoPostEntTabs     = function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;

		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;
		//------------------------------------------------------------------------------------

		//------------------controllers------------------------------------------------------
		var pr_ctr_Main 			= null;
		var pr_ctr_List 			= null;
		var pr_ctr_Ent				= null;
		var pr_ctr_EntTabs 			= null;
		
		//-------------------------------------------------
		var pr_object				= null;
		var pr_mode					= null;
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			pr_ctr_Main 			= App.controller.NsoPost.Main;
			pr_ctr_List 			= App.controller.NsoPost.List;
			
			pr_ctr_Ent				= App.controller.NsoPost.Ent;
			pr_ctr_EntTabDoc 		= App.controller.NsoPost.EntTabDoc;
//			pr_ctr_EntTabRating 			= App.controller.NsoPost.EntTabRating;
			
			tmplName.NSO_POST_ENT_TABS			= "NsoPost_Ent_Tabs";
			
			tmplContr.do_lc_put_tmpl(tmplName.NSO_POST_ENT_TABS, NsoPost_Ent_Tabs); 
		}     
		this.do_lc_show		= function(obj, mode){
			pr_object 	= obj;
			pr_mode		= mode;
			
			var objLink = pr_object;
			var objFile = pr_object;
			
			if(obj && mode != pr_ctr_Main.var_lc_MODE_MOD){
				if(!obj.link || obj.link.length <= 0){
					if(obj.files){
						var files = obj.files;
						var newFiles = [];
						var newLink  = [];
						for(var i = 0; i < files.length; i++){
							if(files[i].typ02 != 3){
								newFiles.push(files[i]);
							}else{
								newLink.push(files[i]);
							}
						}
						objFile.files = newFiles;
						objLink.link  = newLink;
					}
				}
			}
			
			
			try{
							
				$(pr_divContent).html(tmplContr.req_lc_compile_tmpl(tmplName.NSO_POST_ENT_TABS, obj));
				
				//fixed max-height scroll of % height div_ContentView
				do_gl_calculateScrollBody(pr_divContent + " .custom-scroll-tab", 43.6);
						
//				pr_ctr_EntTabRating.do_lc_show(obj, mode);
				pr_ctr_EntTabDoc .do_lc_show(objFile, mode);
//				pr_ctr_EntTabLink.do_lc_show(objLink, mode);
				
				do_bind_event(obj, mode);
			}catch(e) {				
				do_gl_send_exception(App.path.BASE_URL_API, pr_ctr_Main.var_lc_URL_Header, App.network, "nso.post", "NsoPostEntTabs", "do_lc_show", e.toString()) ;
			}
		};
		

		//---------private-----------------------------------------------------------------------------
		var do_bind_event = function (obj, mode){
			
		}

	};


	return NsoPostEntTabs;
});