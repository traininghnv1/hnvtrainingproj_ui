var do_gl_load_JSController_ByRequireJS = function(AppVar, ctrConfig){
	if (!ctrConfig	) return;
	if (!AppVar		) AppVar = {};
	/*
	 AppVar 	= App.controller;
	 ctrConfig 	= {path: "////", nameGroup: "List", name : "Area", initParams: [....], fShow: name, fShowParams : [], fCallBack: function(){},}
	 	nameGroup		: ten cua cac phan lon': area, plan, post, material...
		name			: ten cua controller: list, main, tab...
		path			: duong dan cua controller
		initParams		: cac bien khoi tao cua controller
		fInit 			: ten ham se goi sau khi controller duoc khoi tao
		fInitParams 	: bien cua ham khoi tao
		fShow 			: ten ham se goi sau khi controller duoc khoi tao
		fShowParams 	: bien cua ham khoi tao
		fCallBack 		: nhung ham se thuc hien sau cung
	 */
	try{
		let {nameGroup, name, path, initParams, fInit, fInitParams, fShow, fShowParams, fCallBack} = ctrConfig;
		
		requirejs([path], function(ctrl){			
			if (!AppVar	[nameGroup])
				AppVar	[nameGroup] = {};
			
			if (!AppVar	[nameGroup][name])	
				AppVar	[nameGroup][name]		= new ctrl(...initParams);

			if (!fInitParams)	fInitParams		=  [];
			if (!fShowParams)	fShowParams		=  [];
			if (fInit		) 	AppVar[nameGroup][name][fInit](...fInitParams);
			if (fShow		) 	AppVar[nameGroup][name][fShow](...fShowParams);
			
			if (fCallBack	)	fCallBack();
		})
	}catch(e){
		console.log("do_gl_load_JSController_ByRequireJS:"+ e);
	}	
}

//------function load a js controller
function do_gl_load_JSController(require, ctrPath, grpName, ctrName, newParam01, newParam02, newParam03 ){
	var ctrClass  =  require(ctrPath);
	if (!ctrClass  ){	
		ctrClass  =  require(ctrPath);
	}
	if (!ctrClass  ){	
		ctrClass  =  require(ctrPath);
	}
	if (!ctrClass  ){	
		ctrClass  =  require(ctrPath);
	}
	if (!ctrClass  ){	
		ctrClass  =  require(ctrPath);
	}
	if (!ctrClass  ){	
		console.log("--cannot load "+ctrPath);
		setTimeout(function(param) {
			window.location.reload();
		}, 200);					
		return;
	}
	App.controller[grpName][ctrName]  = new ctrClass(newParam01, newParam02, newParam03);
	App.controller[grpName][ctrName].do_lc_init();
}

//--------------------------------------
function do_gl_sortByKeyIntegerAsc(array, key) {
    return array.sort(function (a, b) {
        var x = parseInt(a[key],10); 
        var y = parseInt(b[key],10);
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}
//----------------------------------------------------
var do_gl_show_HeaderSlider= function( timewait, divSlide, slides, slideIndex) {
	if (!timewait)					timewait = 3000;
	
	if (!slides){
		slides =$(divSlide);
		for (var i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";  
		}
	}
	
	if (!slides)					return;
	if (slides.length==0)			return;
	
	if (!slideIndex) 				slideIndex = 1;
	if (slideIndex>slides.length) 	slideIndex = 1;

	

//	slides[slideIndex-1].style.display = "block";  
	$(slides[slideIndex-1]).fadeIn(1000);

	if (slides.length>1){
		setTimeout(function(){
			$(slides[slideIndex-1]).fadeOut(1000);
			slideIndex++;
			do_gl_show_HeaderSlider (timewait, divSlide, slides, slideIndex);  
		}, timewait);// Change image every 3,5 seconds
	}
}

//---------------------------------------------------
var req_gl_Hex2Array= function(str) {
    if (typeof str == "string") {
        var len = Math.floor(str.length / 2);
        var ret = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            ret[i] = parseInt(str.substr(i * 2, 2), 16);
        }
        return ret;
    }
};