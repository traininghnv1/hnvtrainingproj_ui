/*
var rq_gl_Crypto 						= function(mdp)

var do_gl_Security_Method_Mod 			= function (method)
var req_gl_Security_HttpHeader_New 		= function (login, pass, cryptaMethod)
var do_gl_Security_HttpHeader_Clear 	= function (route, header)
var do_gl_Security_Session_Close 		= function (route)
var do_gl_Security_Session_Open 		= function (route)

var req_gl_Security_Session 			= function (route)
var req_gl_Security_UserProfile 		= function (route)
var req_gl_Security_HttpHeader 			= function (route)

var do_gl_Security_Login_Save  			= function (route, header, userProfile, session_id, remember)
var do_gl_Security_UserProfile_Save  	= function (route, userProfile)
var do_gl_Security_HttpHeader_Save  	= function (route, header, remember)

var do_gl_Security_HttpHeader_Clear 	= function (route, header)

var do_gl_LocalStorage_Save				= function (route, data)
var do_gl_LocalStorage_Clear  			= function (route)
 */

var do_gl_LocalStorage_Save  = function (route, data){
	if (!route)	    route = "tmp";
	if (data) 		localStorage.setItem(SECU_PREFIX+route,JSON.stringify(data));	
}

var do_gl_LocalStorage_Clear  = function (route){
	if (!route)	    route = "tmp";
	localStorage.removeItem(SECU_PREFIX+route);	
}

var req_gl_LocalStorage  = function (route){
	if (!route)	    route = "tmp";
	var data		= localStorage.getItem(SECU_PREFIX+route);
	if (data) data  = JSON.parse(data);
	return data;
}


var rq_gl_Crypto = function(mdp, method) {
	if (!method)
		return makeCrypto_SHA256(mdp);
	else if (method=="sha256")
		return makeCrypto_SHA256(mdp);
	else if (method=="sha512")
		return makeCrypto_SHA512(mdp);
};


var rq_gl_Crypto = function(mdp) {
	return makeCrypto_SHA256(mdp);
};

var makeCrypto_SHA256 = function(mdp) {
	//var crypto = CryptoJS.SHA1(CryptoJS.SHA1(mdp).toString(CryptoJS.enc.Hex)).toString(CryptoJS.enc.Hex);
	//var crypto = sha256_digest(sha256_digest(mdp).toString(CryptoJS.enc.Hex)).toString(CryptoJS.enc.Hex);
	//var crypto = CryptoJS.SHA1(mdp).toString(CryptoJS.enc.Hex);
	var crypto = sha256(mdp);
	return crypto;
};

var makeCrypto_SHA512 = function(mdp) {	
	var crypto = sha512_digest(sha512_digest(mdp).toString(CryptoJS.enc.Hex)).toString(CryptoJS.enc.Hex);	
	return crypto;
};


var SECU_PREFIX='/hnv/';
//App.keys.KEY_STORAGE_CREDENTIAL
//call in LoginControler.js

/*var do_gl_save_Security_Login  = function (route, header, userProfile, session_id, remember){
	storeSecurityLogin (route, header, userProfile, session_id, remember);
}*/

var do_gl_Security_Login_Save  = function (route, header, userProfile, session_id, remember, token){
	storeSecurityLogin (route, header, userProfile, session_id, remember, token);
}

var storeSecurityLogin  = function (route, header, userProfile, session_id, remember, token){
	storeSecurityHttpHeader		(route, header, remember);
	storeSecurityUserProfile	(route, userProfile);
	storeSecuritySession 		(route, session_id);	
	storeSecurityToken	 		(route, token);
}

//call in LoginControler.js
//var do_gl_save_Security_HttpHeader  = function (route, header, remember){
//storeSecurityHttpHeader (route, header, remember);
//}

var do_gl_Security_HttpHeader_Save  = function (route, header, remember){
	storeSecurityHttpHeader (route, header, remember);
}
var storeSecurityHttpHeader  = function (route, header, remember){
	if (header) 		localStorage.setItem(SECU_PREFIX+route,JSON.stringify(header));	
	if (!remember){	
		localStorage.setItem	(SECU_PREFIX+route+ '/rem', JSON.stringify(0));
	}else{		
		localStorage.setItem	(SECU_PREFIX+route+ '/rem', JSON.stringify(1));	
	}

	var d = new Date().getTime();
	localStorage.setItem(SECU_PREFIX+route+ '/time', JSON.stringify(d));
	localStorage.setItem(SECU_PREFIX+route+ '/count','1');
}

//call in LoginControler.js
//var do_gl_save_Security_UserProfile  = function (route, userProfile){
//storeSecurityUserProfile (route, userProfile);
//}

var do_gl_Security_UserProfile_Save  = function (route, userProfile){
	storeSecurityUserProfile (route, userProfile);
}
var storeSecurityUserProfile  = function (route, userProfile){
	if (userProfile) 	localStorage.setItem(SECU_PREFIX+route+ '/usr',JSON.stringify(userProfile));
}

//call in LoginControler.js
//var do_gl_save_Security_Session  = function (route, session_id){
//storeSecuritySession(route, session_id);
//}

var do_gl_Security_Session_Save  = function (route, session_id){
	storeSecuritySession(route, session_id);
}
var storeSecuritySession  = function (route, session_id){
	if (session_id) 	localStorage.setItem(SECU_PREFIX+route+ '/sess',JSON.stringify(session_id));
}

var do_gl_Security_Token_Save  = function (route, token){
	storeSecurityToken(route, token);
}
var do_gl_Security_Token_Ini_Save  = function (route, token){
	storeSecurityTokenIni(route, token);
}
var storeSecurityToken  = function (route, token){
	if (token) 	{
		localStorage.setItem(SECU_PREFIX+route+ '/tok',JSON.stringify(token));
		localStorage.setItem(SECU_PREFIX+route+ '/tok_time', (new Date()).getTime());
	}
}
var storeSecurityTokenIni  = function (route, token){
	if (token) 	localStorage.setItem(SECU_PREFIX+route+ '/tok_ini',JSON.stringify(token));
}

//---------------------------------------------------------------------------------------------
//App.keys.KEY_STORAGE_CREDENTIAL
//call in all controler.js
var TIME_SESS_LIM_REM_0 = 1000*60*60*1;
var TIME_SESS_LIM_REM_2 = 1000*60*60*24*365;
var TIME_TOK_REFRESH	= 60000*1;

var req_gl_Security_Info = function (route){
	var tok_time	= req_gl_Security_Token_Time(route);
	var tok			= req_gl_Security_Token		(route);
	var rem			= req_gl_Security_Remember	(route);
	var secuHeader	= req_gl_Security_HttpHeader(route);
	var user		= req_gl_Security_UserProfile(route);
	
	return {tok_time, tok, rem, secuHeader, user}
}

var req_gl_Security_Token = function (route){
	var tok = localStorage.getItem(SECU_PREFIX+route+ '/tok');
	if (!tok) return null; 
	return JSON.parse(tok);
}

var req_gl_Security_Token_Time = function (route){
	var tok_time = localStorage.getItem(SECU_PREFIX+route+ '/tok_time');
	if (!tok_time) return null; 
	return JSON.parse(tok_time);
}

var req_gl_Security_Remember = function (route){
	var rem = localStorage.getItem(SECU_PREFIX+route+ '/rem');
	if (!rem) return null; 
	return JSON.parse(rem);
}

var req_gl_Security_HttpHeader = function (route){
	var header 	= localStorage.getItem(SECU_PREFIX+route);

	if (!header){
		clearSecurityHttpHeader (route);
		return null;	
	}
	var now 	= new Date().getTime();	
	// check neu da dong het tab hoac browser
	var count  	= localStorage.getItem(SECU_PREFIX+route+ '/count');	
	if (!count) return null;	
	count 	= parseInt(count, 10);

	//check rem
	var rem  	= localStorage.getItem(SECU_PREFIX+route+ '/rem');	
	if (!rem) rem = 0; else rem	= parseInt(rem, 10);


	if (count<=0 && rem==0){			
		var timeClose  	= localStorage.getItem(SECU_PREFIX+route+ '/timeClose');	
		if (timeClose){
			timeClose = parseInt(timeClose,10);

			if (now-timeClose>30000){//30S	
				clearSecurityHttpHeader (route);
				return null;
			}
			localStorage.setItem(SECU_PREFIX+route+ '/count',JSON.stringify(1));

		}else{
			console.log("no time close");
			clearSecurityHttpHeader (route);
			return null;
		}		
	}



	//check time
	var time  	= localStorage.getItem(SECU_PREFIX+route+ '/time');
	if (!time){
		clearSecurityHttpHeader (route);
		return null;
	}

	time 	= parseInt(time, 10);	
	if (rem==0 && now-time>TIME_SESS_LIM_REM_0){ //not remember then 1h max from last session
		clearSecurityHttpHeader (route);
		return null;
	}else if (rem==1 && now-time>TIME_SESS_LIM_REM_2){ //remember then 24h max from last session
		clearSecurityHttpHeader (route);
		return null;
	}else if (rem!=0 && rem!=1){
		clearSecurityHttpHeader (route);
		return null;
	}		

	localStorage.setItem(SECU_PREFIX+route+ '/time' ,JSON.stringify(now));	

	return JSON.parse(header);
}

var req_gl_Security_UserProfile = function (route){
	var userProfile = localStorage.getItem(SECU_PREFIX+route+ '/usr');
	if (!userProfile) return null; 
	return JSON.parse(userProfile);
}

var req_gl_Security_Session = function (route){
	var sess = localStorage.getItem(SECU_PREFIX+route+ '/sess');
	if (!sess) return null; 
	return JSON.parse(sess);
}

var req_gl_Security_Token_Ini = function (route){
	var tok = localStorage.getItem(SECU_PREFIX+route+ '/tok_ini');
	if (!tok) return null; 
	return JSON.parse(tok);
}

var req_gl_Security_Obj = function (route, objName){
	var obj = localStorage.getItem(SECU_PREFIX+route+ '/' + objName);
	if (!obj) return null; 
	return JSON.parse(obj);
}

//-------------------------------------------------------------
//var do_gl_close_Security_Session = function (route){
//doCloseSecuritySession(route);
//}
var do_gl_Security_Session_Close = function (route){
	var count  	= localStorage.getItem(SECU_PREFIX+route+ '/count');	
	if (!count) return null;	
	count 	= parseInt(count,10);
	if (count<=0){
		clearSecurityHttpHeader (route);
		return null;
	}
	var now = new Date().getTime();

	localStorage.setItem(SECU_PREFIX+route+ '/timeClose' ,JSON.stringify(now));	
	localStorage.setItem(SECU_PREFIX+route+ '/count',JSON.stringify(count-1));
}
//-------------------------------------------------------------
//var do_gl_open_Security_Session = function (route){
//doOpenSecuritySession(route);
//}
var do_gl_Security_Session_Open = function (route){
	doOpenSecuritySession(route);
	doSaveTimeLastRequest(route);
}
var doOpenSecuritySession = function (route){
	var count  	= localStorage.getItem(SECU_PREFIX+route+ '/count');	
	if (!count) return null;	
	count 	= parseInt(count,10);	
	localStorage.setItem(SECU_PREFIX+route+ '/count',JSON.stringify(count+1));
}

var doSaveTimeLastRequest= function(route){
	var now 	= new Date().getTime();	
	localStorage.setItem(SECU_PREFIX+route+ '/time' ,JSON.stringify(now));	
}
/*
 window.onbeforeunload = function (event) {
    doCloseSecurity("test");
    return null;
};
 */
//----------

/*
var reqSecurityHttpHeaderAndUserProfile = function (route, cacheDestForProfile){
	var header 		= localStorage.getItem(SECU_PREFIX+route);
	var userProfile = localStorage.getItem(SECU_PREFIX+route+ '/usr');

	if (!header) return false;

	cacheDestForProfile = JSON.parse(userProfile);
	return JSON.parse(header);
}*/

//var do_gl_clear_Security_HttpHeader = function (route, header){
//clearSecurityHttpHeader(route, header);
//}
var do_gl_Security_HttpHeader_Clear = function (route, header){
	clearSecurityHttpHeader(route, header);
}
var clearAllCache = function (){
	localStorage.clear();
}
var clearSecurityHttpHeader  = function (route, header){
	localStorage.removeItem(SECU_PREFIX+route);
	localStorage.removeItem(SECU_PREFIX+route+ '/usr');
	localStorage.removeItem(SECU_PREFIX+route+ '/sess');
	localStorage.removeItem(SECU_PREFIX+route+ '/time');
	localStorage.removeItem(SECU_PREFIX+route+ '/count');
	localStorage.removeItem(SECU_PREFIX+route+ '/tok');

	localStorage.removeItem(SECU_PREFIX+"app_domains");
}

//------------------------------------------------------------------
var SECU_CRYPT_METH=1; //sha256
//var do_gl_set_Security_Method = function (method){
//doSetSecuMethod(method);
//}
var do_gl_Security_Method_Mod = function (method){
	doSetSecuMethod(method);
}
var doSetSecuMethod= function(method){
	SECU_CRYPT_METH = method;
}

//var do_gl_create_Security_HttpHeader = function (login, pass, cryptaMethod){
//return createSecurityHttpHeader(login, pass, cryptaMethod);
//}
var req_gl_Security_HttpHeader_New = function (login, pass, cryptaMethod){
	return createSecurityHttpHeader(login, pass, cryptaMethod);
}
var createSecurityHttpHeader  = function (login, pass, cryptaMethod){
	if (cryptaMethod) doSetSecuMethod(cryptaMethod);
	//var cryptoBasic = login + ":" + makeCrypto_SHA256(pass);
	var cryptoBasic = login + ":" + (pass);
	if (SECU_CRYPT_METH==1){
		cryptoBasic = login + ":" + makeCrypto_SHA256(pass);
	}
	// Create header for ajax call
	var headers = {
			"Authorization": "Basic " + btoa(cryptoBasic)
	};
	return headers;
}

//const salt 				= "577bd45a17977269694908d80905c32a";
//const iv 				= "9a2b73d130c8796309b776eeb09834b0";
class AesUtil {
	constructor(keySize, iterationCount){
		this.keySize 		= keySize / 32;
		this.iterationCount = iterationCount;
	}

	generateKey(salt, passPhrase){
		
		let key = CryptoJS.PBKDF2(passPhrase, salt,
				{ 	
					keySize		: this.keySize, 
					iterations	: this.iterationCount 
				});
		return key;
	}

	encrypt(passPhrase, plainText){
		let salt 		= CryptoJS.lib.WordArray.random(128/8);
		let iv 			= CryptoJS.lib.WordArray.random(128/8);
		
		let key 		= this.generateKey(salt, passPhrase);
		let encrypted 	= CryptoJS.AES.encrypt(plainText, key, {iv});
		
		let transitmessage = salt.toString()+ iv.toString() + encrypted.toString();
		return transitmessage;
	}

	decrypt(passPhrase, cipherText){
		let salt 			= CryptoJS.enc.Hex.parse(cipherText.substr(0, 32));
		let iv 				= CryptoJS.enc.Hex.parse(cipherText.substr(32, 32))
		let encrypted 		= cipherText.substring(64);
		
		let key 			= this.generateKey(salt, passPhrase);
		let cipherParams 	= CryptoJS.lib.CipherParams.create({
			ciphertext: CryptoJS.enc.Base64.parse(encrypted)
		});
		let decrypted = CryptoJS.AES.decrypt(cipherParams, key, {iv});
		return decrypted.toString(CryptoJS.enc.Utf8);
	}
}

var do_gl_encrypt_aes = function(pass, data){
	const iterationCount 	= 1000;
	const keySize 			= 128;
	let aesUtil 			= new AesUtil(keySize, iterationCount);
	let ciphertext 			= aesUtil.encrypt(pass, data);
	return ciphertext;
}

var do_gl_decrypt_aes = function(pass, cipherData){
	const iterationCount 	= 1000;
	const keySize 			= 128;
	let aesUtil 			= new AesUtil(keySize, iterationCount);
	let decrypText  		= aesUtil.decrypt(pass, cipherData);
	return decrypText ;
}
