/**
 * JS to manage lock functionalities.
 */

define(['jquery'],
	function($){
		var LockTool 	= function () {
			
			/**
			 * Method to check whether any lock is available.
			 */
			this.checkLock = function(){
				if(App.data.lock_status){
					App.msgBox.show({
						title	: $.i18n('lock_tool_err_save_title') ,
						content	: $.i18n('lock_tool_err_msg')
					});	
				}
				return App.data.lock_status;
			}
			
			/**
			 * Method to set lock parameter.
			 */
			this.setLock = function(lockStatus){
				App.data.lock_status = lockStatus;
			}
			
			/**
			 * Method to get the lock status.
			 */
			this.can_lc_get_lock = function(){
				return App.data.lock_status;
			}
		};
		 return LockTool;
	}
);