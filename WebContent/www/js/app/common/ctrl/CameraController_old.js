 define([
        'jquery',
        'text!template/common/CMS_Camera.html'
        ],
        function($, 
        		TmplCamera) {

	var CameraController = function(customSelector) {
		var pluginLocation	= "www/js/lib/jpegCamera/";
		var tmplName		= App.template.names;
		var tmplContr		= App.template.controller;
		
		
		var self = this;

		var camera; // Initialized at the end

		var defaultSelector = {};
		defaultSelector["div_camera_info"		]	= "#camera_info";
		defaultSelector["div_stream_stats"		]	= "#stream_stats";
		defaultSelector["div_camera"			]	= "#camera";
		defaultSelector["div_take_snapshots"	]	= "#take_snapshots";
		defaultSelector["div_take_snapshots"	]	= "#take_snapshots";
		defaultSelector["div_show_stream"		]	= "#show_stream";
		defaultSelector["div_snapshots"			]	= "#snapshots";
		defaultSelector["div_discard_snapshot"	]	= "#discard_snapshot";
		defaultSelector["div_upload_snapshot"	]	= "#upload_snapshot";
		defaultSelector["div_api_url"			]	= "#api_url";
		defaultSelector["div-loader"			]	= "#loader";
		defaultSelector["div_upload_status"		]	= "#upload_status";
		defaultSelector["div_upload_result"		]	= "#upload_result";
		

		if(!customSelector)	customSelector = {};
		var selector = $.extend(true, {}, defaultSelector, customSelector);

		this.capture_Ava		= null;
		
		try {
			this.do_lc_init = function() {
				tmplContr.do_lc_put_tmpl(tmplName.CMS_CAMERA				, TmplCamera);
			}

			this.do_lc_show = function(fileinputObj, avatar) {
				self.do_lc_init();
				App.MsgboxController.do_lc_show({
					title		: "",
					content 	: tmplContr.req_lc_compile_tmpl(tmplName.CMS_CAMERA, {}),
					buttons		: "none",
					autoclose	: false,
					width		: "90vw",
				    height		: "80vh",
					bindEvent: function() {
					}
				});
				
				self.capture_Ava	= avatar;
				
				self.do_lc_start(fileinputObj);
			}

			this.do_lc_start = function(fileinputObj) {
				var options = {
//						shutter_ogg_url	: pluginLocation+"shutter.ogg",
//						shutter_mp3_url	: pluginLocation+"shutter.mp3",
//						swf_url			: pluginLocation+"jpeg_camera.swf"
				}

				camera = new JpegCamera(selector.div_camera, options).ready(function(info) {
					$(selector.div_take_snapshots).show();

					$(selector.div_camera_info).html(
							"Camera resolution: " + info.video_width + "x" + info.video_height);

					this.get_stats(self.do_lc_update_stream_stats);
				});

				$(selector.div_take_snapshots).click(function() {self.do_lc_take_snapshots(1);});
				$(selector.div_snapshots).on("click", ".item", self.do_lc_select_snapshot);
				$(selector.div_upload_snapshot).click(function() {self.do_lc_upload_snapshot(fileinputObj);});
				$(selector.div_discard_snapshot).click(self.do_lc_discard_snapshot);
				$(selector.div_show_stream).click(self.do_lc_show_stream);
				
				self.do_lc_hide_snapshot_controls();
			}
			
			this.do_lc_stop = function() {
				camera._engine_stop();
			}


			this.do_lc_update_stream_stats = function(stats) {
				$(selector.div_stream_stats).html(
						"Mean luminance = " + stats.mean +
						"; Standard Deviation = " + stats.std);

				setTimeout(function() {camera.get_stats(self.do_lc_update_stream_stats);}, 1000);
			};

			this.do_lc_take_snapshots = function(count) {
				var snapshot = camera.capture();

				if (JpegCamera.canvas_supported()) {
					snapshot.get_canvas(self.do_lc_add_snapshot);
				}
				else {
					// <canvas> is not supported in this browser. We'll use anonymous
					// graphic instead.
					var image = document.createElement("img");
					image.src = pluginLocation+"no_canvas_photo.jpg";
					setTimeout(function() {self.do_lc_add_snapshot.call(snapshot, image)}, 1);
				}

				if (count > 1) {
					setTimeout(function() {self.do_lc_take_snapshots(count - 1);}, 500);
				}
			};

			this.do_lc_add_snapshot = function(element) {
				$(element).data("snapshot", this).addClass("item");

				var $container = $(selector.div_snapshots).append(element);
				var $camera = $(selector.div_camera);
				var camera_ratio = $camera.innerWidth() / $camera.innerHeight();

				var height = $container.height()
				element.style.height = "" + height + "px";
				element.style.width = "" + Math.round(camera_ratio * height) + "px";

				var scroll = $container[0].scrollWidth - $container.innerWidth();

				$container.animate({
					scrollLeft: scroll
				}, 200);
				
				self.do_lc_show_snapshot_controls();
			};

			this.do_lc_select_snapshot = function () {
				$(".item").removeClass("selected");
				var snapshot = $(this).addClass("selected").data("snapshot");
//				self.do_lc_show_snapshot_controls();
				snapshot.show();
//				$(selector.div_show_stream).show();
			};

			this.do_lc_clear_upload_data = function() {
				$(selector.div_upload_status + "," + selector.div_upload_result).html("");
			};

			this.do_lc_upload_snapshot = function(fileinputObj) {
				var api_url = fileinputObj; //$(selector.div_api_url).val();

//				if (!api_url.length) {
//					$(selector.div_upload_status).html("Please provide URL for the upload");
//					return;
//				}

				self.do_lc_clear_upload_data();
//				$(selector.div-loader).show();
//				$(selector.div_upload_snapshot).prop("disabled", true);
				
				var i01 = $(selector.div_snapshots).find("canvas.item.selected");
				var i02 = $(selector.div_snapshots).find("canvas.item");
				
				if (i01.length == 0) {
					if(self.capture_Ava == true) {
						alert("Please select only one image to use avatar!");
						return;
					}
					
					i01 = i02;

				}
				if (i02.length == 0) return;
				
				for (var i=0; i<i01.length; i++){
					try {
						var snapshot = $(i01[i]).data("snapshot");
//						snapshot.upload({api_url: api_url}).done(self.do_lc_upload_done).fail(self.do_lc_upload_fail);
						if(snapshot)	snapshot.upload_fileinput(fileinputObj).done(self.do_lc_upload_done).fail(self.do_lc_upload_fail);
			
					} catch (e) {
						console.log(e);
					}
					
				}
				fileinputObj.refresh();
				App.MsgboxController.do_lc_close();
				self.do_lc_stop();
		};

			this.do_lc_upload_done = function(response) {
//				$(selector.div_upload_snapshot).prop("disabled", false);
//				$(selector.div-loader).hide();
//				$(selector.div_upload_status).html("Upload successful");
//				$(selector.div_upload_result).html(response);
			};

			this.do_lc_upload_fail = function(code, error, response) {
//				$(selector.div_upload_snapshot).prop("disabled", false);
//				$(selector.div-loader).hide();
//				$(selector.div_upload_status).html(
//						"Upload failed with status " + code + " (" + error + ")");
//				$(selector.div_upload_result).html(response);
				
				alert("Retry");
			};

			this.do_lc_discard_snapshot = function() {
				//first element is selected
				var lstEle = $(selector.div_snapshots).find("canvas");
				if (lstEle.length > 0) $(lstEle[0]).addClass("selected");
				
				var element = $(".item.selected").removeClass("item selected");

				var next = element.nextAll(".item").first();

				if (!next.size()) {
					next = element.prevAll(".item").first();
				}

				if (next.size()) {
					next.addClass("selected");
					next.data("snapshot").show();
				}
				else {
					self.do_lc_hide_snapshot_controls();
				}

				element.data("snapshot").discard();

				element.hide("slow", function() {$(this).remove()});
			};

			this.do_lc_show_stream = function() {
				$(this).hide();
				$(".item").removeClass("selected");
				self.do_lc_hide_snapshot_controls();
				sef.do_lc_clear_upload_data();
				camera.show_stream();
			};
			
			this.do_lc_show_snapshot_controls = function() {
				$(selector.div_discard_snapshot + "," + selector.div_upload_snapshot + "," + selector.div_api_url).show();
			};

			this.do_lc_hide_snapshot_controls = function() {
				$(selector.div_discard_snapshot + "," + selector.div_upload_snapshot + "," + selector.div_api_url).hide();
				$(selector.div_upload_result + "," + selector.div_upload_status).html("");
				$(selector.div_show_stream).hide();
			};


		} catch (e) {
			console.log(e);
		}

	}

	return CameraController;

});


