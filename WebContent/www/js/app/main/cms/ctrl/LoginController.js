define(['jquery',        
	'text!main/cms/tmpl/Login_Content.html',
	'text!main/cms/tmpl/Register_Content.html'
	],
	function($, LOGIN_CONTENT,
				REGISTER_CONTENT) {
	var LoginControllerPrj = function (FIRST_VIEW) {
		//---------------------------------------
		window.onbeforeunload = function (event) {
			doCloseSecuritySession(App.keys.KEY_STORAGE_CREDENTIAL);
			return null;
		};
		//---------------------------------------
		this.URL_LOGIN  = "https://localhost:44335/api/login";
		this.viewAfter	= FIRST_VIEW;

		var tmplName		= App.template.names;
		var tmplContr		= App.template.controller;
		var self 			= this;

		var svClass 		= AppCommon['const'].SV_CLASS;
		var svName			= AppCommon['const'].SV_NAME;
		var sessId			= AppCommon['const'].SESS_ID;
		var fVar			= AppCommon['const'].FUNCT_SCOPE;
		var fName			= AppCommon['const'].FUNCT_NAME;
		var fParam			= AppCommon['const'].FUNCT_PARAM;

		var pr_setTime_refresh_login 		= null;

		var pr_new_obj_default		= {
				typ : 5,
				per : {
					typ01 : 0,
					typ02 : 0
				}
		}

		this.do_lc_show = function(){
			self.do_lc_Logout		();
			self.do_lc_appVersion 	();
			
			tmplName.LOGIN_CONTENT		= "LOGIN_CONTENT";
			tmplName.REGISTER_CONTENT	= "REGISTER_CONTENT";
			
			tmplContr.do_lc_put_tmpl(tmplName.LOGIN_CONTENT, LOGIN_CONTENT);             
			var compiledContent = tmplContr.req_lc_compile_tmpl (tmplName.LOGIN_CONTENT, {"version" : appVersion});  
			$("#layout-wrapper, #right-bar, #rightbar-overlay").html("");
			$("#login_page").html(compiledContent);

			bindingEventsPage();
		};

		//----------------------------------------------------------------------------------
		this.do_lc_Logout = function (){
			if (!App.data.user) return;

			var ref 			= {};

			pr_setTime_refresh_login && clearInterval(pr_setTime_refresh_login); // clear interval 

			var header 			= {"Content-Type": "application/json"};
			ref["login"		]	= App.data.user.login;
			ref["token"		]	= req_gl_Security_HttpHeader (App.keys.KEY_STORAGE_CREDENTIAL);

			App.network.do_lc_ajax_bg (App.path.BASE_URL_API + App.path.API_LOGOUT, header, ref, 100000, null, null) ;	

			if (App){
				App.data ={};
				App.data.session_id	= -1;    	
			}

			do_gl_Security_HttpHeader_Clear(App.keys.KEY_STORAGE_CREDENTIAL);

			if (!localStorage.language) localStorage.language = "en";					
		}
		//----------------------------------------------------------------------------------
		var do_login_submit = function () {  
			//logout everything first
			self.do_lc_Logout();

			App.network.startLoader();

			var login 		= $("#inp_Username").val();		
			var password 	= $("#inp_Password").val();
			var remember 	= $("#inp_remember").is(":checked");	
//			password 		= rq_gl_Crypto(password);
			
			self.do_lc_Login (login, password, "", remember, self.viewAfter);

			App.network.stopLoader();                   
		}.bind(this);


		this.do_lc_Login = function (login, pass, salt,  remember, viewToShow , background){
			var pass_hash = rq_gl_Crypto(pass+salt);

			var	newConn			= true;			
			var ref 			= {};
			ref[svClass		] 	= "/login"; 
			ref[svName		]	= "svAutLogin"; 	//return user with rights + sessionId
//			ref["salt"		]	= salt;  			//App.data.session_id;
			ref["user_name"	] 	= login; 
			ref["user_pass"	] 	= pass_hash; 			

			var fSucces		= [];
			fSucces.push(req_gl_funct(App	, App.funct.put		, ['user']));
			fSucces.push(req_gl_funct(null	, do_loadView		, [null, viewToShow, true, login,  pass, remember]));

			var fError 		= req_gl_funct(null, reset, []);

			var header 		= {"Content-Type": "application/json"};

			if (!background) 
				App.network.do_lc_ajax (App.path.BASE_URL_API + App.path.API_LOGIN, header, ref, 100000, fSucces, fError) ;
			else 
				App.network.do_lc_ajax_bg (App.path.BASE_URL_API + App.path.API_LOGIN, header, ref, 100000, fSucces, fError) ;
			
			self.do_lc_Login_Interval(login, pass, remember);
		}

		this.do_lc_Login_Interval = function (login, pass, remember ){
			pr_setTime_refresh_login && clearInterval(pr_setTime_refresh_login); // clear interval

			pr_setTime_refresh_login = setInterval(() => { //and reset interval
				let userProfile = req_gl_Security_UserProfile(App.keys.KEY_STORAGE_CREDENTIAL);
				var header		= req_gl_Security_HttpHeader(App.keys.KEY_STORAGE_CREDENTIAL);			

				if (userProfile && header) {
					self.do_lc_Login(login, pass, "", remember,  null, true);	
				} else {
					self.do_lc_Logout();
				}

			}, TIME_TOK_REFRESH);
		}

		this.do_lc_show_View = function (viewToShow, login, pass, remember){
			do_loadView(null, null, viewToShow, false, login, pass);	
			
			self.do_lc_Login_Interval(login, pass, remember);
		}



		// Bind events in the page
		var bindingEventsPage = function() {             	
//			$("#frm_Login").validate({
//			submitHandler: submit
//			});

			//binding enter key for password input
			$("#inp_Password").keyup(function (e) {
				if (e.keyCode === 13) {
					do_login_submit();
				}
			});

			$("#btn_Submit").off("click").on("click", function () {
				do_login_submit();
			});

			$("#btn_register").off("click").on("click", function(){
				do_lc_showForm_register();
			})
		}.bind(this);

		var reset = function(){
			pr_setTime_refresh_login && clearInterval(pr_setTime_refresh_login);
			do_gl_show_Notify_Msg_Error ($.i18n("login_err_authentification"));
			localStorage.clear();				
			App.router.controller.do_lc_run(App.router.routes.CMS_MAIN);
		}

		var do_loadView = function(sharedJson, header, view, newConn, login, pass, remember){
			if((sharedJson && sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) || !sharedJson) {	

				//Check visitor / Guest denied access:
				if(App.data.user){
					if(self.can_lc_User_Guest() || self.can_lc_User_Client_Public()) {
						do_gl_show_Notify_Msg_Error($.i18n("common_access_deny_for_client_user"));
						App.router.controller.do_lc_run(App.router.routes.LOGOUT+'/'+App.router.routes.HOME_VIEW);
						return;
					}
				}

				if (!App.data.user.id) {
					let userProfile = req_gl_Security_UserProfile(App.keys.KEY_STORAGE_CREDENTIAL);
					if (userProfile) App.data.user = userProfile;
				}

				if (typeof  App.data.user === 'string' ) App.data.user = JSON.parse (App.data.user);
				
				//Save the user idman
				if (App.data.user.per) 
					localStorage.setItem("manId", App.data.user.per.parent);

				if (header==""||!header) {
					if (sharedJson) {
						header = sharedJson.user_tok;
					} else {
						header = req_gl_Security_HttpHeader(App.keys.KEY_STORAGE_CREDENTIAL);
					}
				} 
				App.data.user.headerURLSecu = header;

				if (newConn) {
					do_gl_Security_Login_Save		(App.keys.KEY_STORAGE_CREDENTIAL, header, App.data.user, App.data.session_id, remember, pass)
				}else{
					do_gl_Security_Session_Open		(App.keys.KEY_STORAGE_CREDENTIAL);
				}

				if (!view) return;
				
				App.router.controller.do_lc_run(view);
				
			} else {
				do_gl_show_Notify_Msg_Error($.i18n("login_err_authentification"));
			}
		};

		var do_lc_showForm_register = function(){
			self.do_lc_Logout();

			$("#layout-wrapper, #right-bar, #rightbar-overlay").html("");
			tmplContr.do_lc_put_tmpl(tmplName.REGISTER_CONTENT, REGISTER_CONTENT);             
			$("#login_page")		.html(tmplContr.req_lc_compile_tmpl (tmplName.REGISTER_CONTENT, {"version" : appVersion}));

			do_lc_bin_event_register();
		}

		var do_lc_bin_event_register = function(){
			$("#btn_Register").off("click").on("click", function () {
				var obj_register = req_gl_data({
					dataZoneDom : $("#div_sendRegister")
				});

				if(obj_register.hasError){
					do_gl_show_Notify_Msg_Error($.i18n ("content_register_error"));
					return; 
				}

				obj_register.data.pass 			= rq_gl_Crypto(obj_register.data.pass);
				obj_register.data				= $.extend(true, {}, obj_register.data, pr_new_obj_default);			

				obj_register.data.per.info10 = obj_register.data.email;
				doRegisterUser(obj_register);
			});
		}

		var doRegisterUser = function(obj_register) { 
			var ref = req_gl_Request_Content_Send("ServiceAutUser", "SVAutUserNew");

			do_gl_Security_HttpHeader_Clear();
			var header 		= req_gl_Security_HttpHeader_New ("visitor", "visitor");
			var sId			= -1;

			var fSucces = [];
			fSucces.push(req_gl_funct(null, showResultRegister, [ ref ]));

			var fError = req_gl_funct(null, do_gl_show_Notify_Msg_Error, [$.i18n("common_err_ajax") ]);
			App.network.ajax (App.path.BASE_URL_API, header, ref, 100000, fSucces, fError) ;	
		}

		var showResultRegister = function (sharedJson, reference) {
			if (sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
				do_gl_show_Notify_Msg_Success($.i18n ("content_register_succes"));
				do_Show_Login_Popup(false);
			} else {
				do_gl_show_Notify_Msg_Error($.i18n ("content_register_error"));
			}
		}

		var do_Show_Login_Popup = function (stat) {
			if(stat) {
				$(App.constHTML.id.LOGIN_VIEW)	.addClass('open');
				$('body')						.addClass('wygo-hidescroll');
				$( "#inp_Username" ).focus();
			} else {
				$(App.constHTML.id.LOGIN_VIEW)	.removeClass('open');
				$('body')						.removeClass('wygo-hidescroll');
			}
		}

		//--------------------------------------------------------------------------------------------
		//check current user is visitor or client public
		this.can_lc_User_Guest = function() {
			return App.data.user.typ == 1 ? true : false;
		}
		//check current user is client public
		this.can_lc_User_Client_Public = function() {
			return App.data.user.typ == 5 ? true : false;
		}

		//--------------------------------------------------------------------------------------------
		this.do_lc_appVersion = function(){
			var header 			= req_gl_Security_HttpHeader_New ("visitor", "visitor");

			var ref 			= {};
			ref[svClass		] 	= "/appVer"; 
			ref[svName		]	= "svAppVersion"; 

			var fSucces		= [];
			var f01 = {}; 	f01[fVar]	= null; 	f01[fName] = do_lc_saveAppVersion;	f01[fParam]=[];
			fSucces.push(f01);

			var header 		= {"Content-Type": "application/json"};
			App.network.ajax (App.path.BASE_URL_API + "/appVer", header, ref, 100000, fSucces, null) ;

		}
		var do_lc_saveAppVersion = function (sharedJson){
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
				let appVers 		= sharedJson[App['const'].RES_DATA];
				localStorage.setItem("appVersion", appVers);
				if (appVersion == "1.25") return;
				if (appVers != appVersion){
					window.location.reload();
				}
			} else {
			}
		}
	};
	return LoginControllerPrj;
});