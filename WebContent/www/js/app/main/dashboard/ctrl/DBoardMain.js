define(['handlebars',
	'text!main/dashboard/tmpl/DBoard_Main.html',
	'text!main/dashboard/tmpl/DBoard_Rightbar.html',

	'main/dashboard/ctrl/DBoardCommon',
	],
	function (handlebars,
			DBoard_Main,
			DBoard_Rightbar,
			{
				DBoardHeader,
				DBoardNotification,
				DBoardSidebar
			}
	) {
	const DBoardMain = function (header, content, footer) {
		const tmplName 	= App.template.names;
		const tmplContr = App.template.controller;
		var self 		= this;
		//------------------------------------------------------------------------------------
		this.pr_LST_USER_ONLINE 	= [];
		//--------------------APIs--------------------------------------//
		this.do_lc_init = function () {
			tmplName.DBOARD_MAIN 		= "DBoard_Main";
			tmplName.DBOARD_RIGHTBAR 	= "DBoard_Rightbar";
			//-----------------------------------------------------------------------------------------------------------
			loadContructor();
		}

		const loadContructor = () => {
			const pr_CONTROLLER = { Notify: DBoardNotification, Sidebar: DBoardSidebar, Header: DBoardHeader };

			if (!App.controller.DBoard) App.controller.DBoard = {};
			
			for (let name in pr_CONTROLLER) {
				if (!App.controller.DBoard[name]) {
					App.controller.DBoard[name] = new pr_CONTROLLER[name](null, null, null);
					App.controller.DBoard[name].do_lc_init();
				}
			}

			tmplContr.do_lc_put_tmpl(tmplName.DBOARD_MAIN		, DBoard_Main);
			tmplContr.do_lc_put_tmpl(tmplName.DBOARD_RIGHTBAR	, DBoard_Rightbar);
		}

		this.do_lc_show = function () {
			try {
				$("#layout-wrapper").html(tmplContr.req_lc_compile_tmpl(tmplName.DBOARD_MAIN, {}));
				$("#div_right_bar").html(tmplContr.req_lc_compile_tmpl(tmplName.DBOARD_RIGHTBAR, { url: UI_URL_ROOT }));

				App.controller.DBoard.Header.do_lc_show();
				App.controller.DBoard.Sidebar.do_lc_show();
				do_lc_show_content();

				$("#login_page").html("");
				do_gl_bind_page();
			} catch (e) {
				console.log(e);
			}
		};

		this.do_lc_switch_mobile_or_pc = function (url, view_part, paramsShow) {
			window.open(url, "_self");
		}

		const do_lc_show_content = paramsShow => {
			if (paramsShow) pr_DATA_CTRL[VIEW_PART].fShowParams = paramsShow;
			do_gl_load_JSController_ByRequireJS(App.controller, pr_DATA_CTRL[VIEW_PART]);
		}

		const pr_DATA_CTRL = {
				
				[App.router.part.VI_PER_PERSON]: {
					nameGroup: "Per", name: "PerPerson",
					path: "group/per/person/ctrl/PerMain", initParams: [null, "#div_main_content", null],
					fInit: "do_lc_init", fInitParams: [],
					fShow: "do_lc_show", fShowParams: [],
				},
				[App.router.part.VI_PER_INFO]: {
					nameGroup: "Per", name: "PerInfo",
					path: "controller/shp/perInfo/PerMain", initParams: [null, "#div_main_content", null],
					fInit: "do_lc_init", fInitParams: [],
					fShow: "do_lc_show", fShowParams: [],
				},
				[App.router.part.VI_AUT_USER]: {
					nameGroup: "Aut", name: "AutUser",
					path: "group/aut/user/ctrl/AutUserMain", initParams: [null, "#div_main_content", null],
					fInit: "do_lc_init", fInitParams: [],
					fShow: "do_lc_show", fShowParams: [],
				},
				
				
				[App.router.part.VI_TPY_CATEGORY]: {
					nameGroup: "Tpy", name: "TpyCategory",
					path: "group/tpy/category/ctrl/TpyCategoryMain", initParams: [null, "#div_main_content", null],
					fInit: "do_lc_init", fInitParams: [],
					fShow: "do_lc_show", fShowParams: [],
					fCallBack: function () { }
				},
				
				[App.router.part.VI_NSO_POST]: {
					nameGroup: "Nso", name: "NsoPost",
					path: "group/nso/post/ctrl/NsoPostMain", initParams: [null, "#div_main_content", null],
					fInit: "do_lc_init", fInitParams: [],
					fShow: "do_lc_show", fShowParams: [],
				},
				
				[App.router.part.VI_AUT_RIGHT]: {
					nameGroup: "Aut", name: "AutRight",
					path: "group/aut/right/ctrl/AutRightMain", initParams: [null, "#div_main_content", null],
					fInit: "do_lc_init", fInitParams: [],
					fShow: "do_lc_show", fShowParams: [],
				},
				
				[App.router.part.VI_AUT_ROLE]: {
					nameGroup: "Aut", name: "AutRole",
					path: "group/aut/role/ctrl/AutRoleMain", initParams: [null, "#div_main_content", null],
					fInit: "do_lc_init", fInitParams: [],
					fShow: "do_lc_show", fShowParams: [],
				},
				
		}

		//---------------------------FORMAT DATE TIME TOOL--------------------------------------------
		this.formatShortDate 	= (date) 			=> handlebars.helpers.reqFormatShortDate(date);// dd/MM/yyyy
		this.formatDate 		= (date) 			=> handlebars.helpers.reqFormatDate(date); // dd/MM/yyyy hh:mm:ss
		this.formatDateHHMM		= (date) 			=> handlebars.helpers.reqFormatDateHHMM(date); // dd/MM/yyyy hh:mm:ss
		this.do_show_Notify_Msg = (sharedJson, msg) => console.log("do_show_Msg::" + msg);
		this.do_show_Msg 		= (sharedJson, msg) => console.log("do_show_Msg::" + msg);

		this.do_lc_bind_event_div_MaxResize = function (div_left, div_right, btnId) {
			if (!div_left)	div_left 	= "#div_List";
			if (!div_right)	div_right 	= "#div_Ent";
			if (!btnId) 	btnId 		= "#vertical-list-btn";
			
			$(btnId).off("click").on("click", function (e) {
				e.preventDefault();
				$(div_left)		.toggleClass('col-lg-3 col-md-3').toggleClass('col-lg-6 col-md-6');
				$(div_right)	.toggleClass('col-lg-9 col-md-9').toggleClass('col-lg-6 col-md-6');
			})
		}
		
		this.do_lc_bind_event_div_MinResize = function (div_left, div_right, btnClass) {
			if (!div_left)	div_left 	= "#div_List";
			if (!div_right)	div_right 	= "#div_Ent";
			if (!btnClass) 	btnClass 	= ".btn-minimize-list";
			
			$(btnClass).off("click").on("click", function () {
				let $this 			= $(this);
				let { divtoogle } 	= $this.data();
				let child			= $this.find("i");
				let label 			= $this.find(".label-resize");
				child		.toggleClass("mdi-window-minimize mdi-window-maximize")
				$(divtoogle).toggle("hide");

				$('#vertical-list-btn').toggleClass("hide");

				const $div_left = $(div_left);
				$div_left.hasClass('col-lg-3 col-md-3') ? $div_left.removeClass('col-lg-3 col-md-3') :
				$div_left.hasClass('col-lg-6 col-md-6') ? $div_left.removeClass('col-lg-6 col-md-6') : 
				$div_left.addClass('col-lg-3 col-md-3');
				
				$div_left.toggleClass('col-lg-1 col-md-1');

				const $div_right = $(div_right);
				$div_right.hasClass('col-lg-9 col-md-9') ? $div_right.removeClass('col-lg-9 col-md-9') :
				$div_right.hasClass('col-lg-6 col-md-6') ? $div_right.removeClass('col-lg-6 col-md-6') : 
				$div_right.addClass('col-lg-9 col-md-9');
				
				$div_right.toggleClass('col-lg-11 col-md-11');

				label.html(child.hasClass("mdi-window-minimize") ? $.i18n("prj_project_resize_min") : $.i18n("prj_project_resize_max"));
			})
		}

		this.do_lc_bind_event_div_Minimize = function (btnClass) {
			if (!btnClass) btnClass = ".btn-resize";
			$(btnClass).off("click").on("click", function () {
				let $this 			= $(this);
				let { divtoogle } 	= $this.data();
				let child 			= $this.find("i");
				let label 			= $this.find(".label-resize");
				child		.toggleClass("mdi-window-minimize mdi-window-maximize")
				$(divtoogle).toggle("hide");

				label		.html(child.hasClass("mdi-window-minimize") ? $.i18n("prj_project_resize_min") : $.i18n("prj_project_resize_max"));
			})
		}

		
		
		this.do_lc_minimize = function (div){
			if (!div)	return;
			
			for (var i in div){
				var div 			= div[i] + " .btn-resize";
				let $this 			= $(div);
				if ($this.length==0) continue;
				let {divtoogle} 	= $this.data();
				let child 			= $this.find("i");
				let label 			= $this.find(".label-resize");
				child		.toggleClass("mdi-window-minimize mdi-window-maximize")
				$(divtoogle).toggle("hide");

				label		.html(child.hasClass("mdi-window-minimize") ? $.i18n("prj_project_resize_min") : $.i18n("prj_project_resize_max"));
			}
		}
		
		var var_lc_MODE_INIT 		= 0;
		var var_lc_MODE_NEW 		= 1; //duplicate is the mode new after clone object
		var var_lc_MODE_MOD 		= 2;
		var var_lc_MODE_DEL 		= 3;	
		var var_lc_MODE_SEL 		= 5;
		this.do_lc_prevent_winClosing = function (mode){
			if (mode == var_lc_MODE_NEW || mode == var_lc_MODE_MOD){
				window.onbeforeunload = function (event) {
					return true;
				};
			}else{
				window.onbeforeunload = function (event) {
					return null;
				};
			}
		}
	};

	return DBoardMain;
});