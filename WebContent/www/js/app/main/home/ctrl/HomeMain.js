define([
		'require','jquery',
        
        'text!main/home/tmpl/Home_Main.html',
        
        'main/home/ctrl/HomeList',
        'main/home/ctrl/HomeEnt',
        'main/home/ctrl/HomeEntFunction'
        ],
        function(	require, $,         		
        			Home_Main, HomeList, HomeEnt, HomeEntFunction
        ) {

	var HomeMain 	= function (header,content,footer) {
		var pr_divHeader 			= header;
		var pr_divContent 			= content;
		var pr_divFooter 			= footer;
		
		//------------------------------------------------------------------------------------
		var tmplName				= App.template.names;
		var tmplContr				= App.template.controller;
		
		var svClass 				= App['const'].SV_CLASS;
		var svName					= App['const'].SV_NAME;
		var sessId					= App['const'].SESS_ID;
		var userId          		= App['const'].USER_ID;

		var fVar					= App['const'].FUNCT_SCOPE;
		var fName					= App['const'].FUNCT_NAME;
		var fParam					= App['const'].FUNCT_PARAM;		

		var self 					= this;		
		//------------------------------------------------------------------------------------
		
		this.var_lc_MODE_INIT 		= 0;
		this.var_lc_MODE_NEW 		= 1; //duplicate is the mode new after clone object
		this.var_lc_MODE_MOD 		= 2;
		this.var_lc_MODE_DEL 		= 3;	
		this.var_lc_MODE_SEL 		= 5;
	
		this.var_lc_URL_Home_Header	= null;
		//---------------------------------------------------------------
		
					
		//--------------------APIs--------------------------------------//
		this.do_lc_init		= function(){
			if (!HomeList || !HomeEnt || !HomeEntFunction){	
				setTimeout(function() {					
					window.location.reload();
				}, 100);					
				return;
			}
			
			if (!App.controller.Home)				
				 App.controller.Home				= {};
			
			if (!App.controller.Home.Main)	
				App.controller.Home.Main 		= this; //important for other controller can get ref, when new this controller,
			
			if (!App.controller.Home.List		)  
				 App.controller.Home.List		= new HomeList			(null, "#div_Home_List_Bookmark" , null);				
			if (!App.controller.Home.Ent		)  
				 App.controller.Home.Ent		= new HomeEnt			(null, "#div_Home_Ent" , null);
			if (!App.controller.Home.EntFunct	)  
				 App.controller.Home.EntFunct	= new HomeEntFunction	(null, "#div_Home_Ent_Function" , null);

			//--------------------------------------------------------------------------------------------------
			
			self.var_lc_URL_Aut_Header			= req_gl_Security_HttpHeader (App.keys.KEY_STORAGE_CREDENTIAL);
			
			App.controller.Home.List		.do_lc_init();
			App.controller.Home.Ent			.do_lc_init();
			App.controller.Home.EntFunct	.do_lc_init();
			
			tmplName.HOME_MAIN			= "Home_Main";
			tmplName.HOME_LIST			= "Home_List";
			tmplName.HOME_LIST_CONTENT	= "Home_List_Content";
			tmplName.HOME_ENT			= "Home_Ent";
			tmplName.HOME_ENT_FUNCTION	= "Home_Ent_Function";
			
		}
		
		
		this.do_lc_show = function(){
			try { 
				self.var_lc_URL_Aut_Header				= req_gl_Security_HttpHeader (App.keys.KEY_STORAGE_CREDENTIAL);
				
				tmplContr.do_lc_put_tmpl(tmplName.HOME_MAIN	, Home_Main); 
				$(pr_divContent).html(tmplContr.req_lc_compile_tmpl(tmplName.HOME_MAIN, {}));	
				
//				do_get_list_domain();
				
				if(App.data.domains) {
					App.controller.Home.Ent	.do_lc_show(App.data.domains);
					App.controller.Home.List.do_lc_show(App.data.domains);
				}else{
					if (pr_domain_requesting){
						setTimeout(function(){
							self.do_lc_show();
						},200);
					}else{
						self.req_domain_list (self.do_lc_show);
					}
				}
	
			} catch(e) {
				console.log(e);
				do_gl_send_exception(App.path.BASE_URL_API, self.var_lc_URL_Aut_Header, App.network, "home", "HomeMain", "do_lc_show", e.toString()) ;
			}
		};
		
		this.do_lc_binding_pages = function(div) {
			try {
				if(div.length>0) do_gl_enhance_within(div);
			} catch (e) {
				self.do_show_Msg(null, e);
			}
		};
		
		this.req_domain_list = function(callback) {
			
			if(App.data.domains) {
				callback(App.data.domains);
			} else {
				var data = req_gl_LocalStorage("app_domains");
				if (data){
					App.data.domains = data;
					callback(App.data.domains);
				}else{
					do_get_list_domain(callback);
				}				
			}
		}
		
		var pr_domain_requesting = false;
		//---------private-----------------------------------------------------------------------------
		var do_get_list_domain= function(callback){
			//ajax to get all fix values here			
			var ref 		= req_gl_Request_Content_Send('ServiceSysHome', 'SVSysDomainLst');			
		
			var fSucces		= [];		
			fSucces.push(req_gl_funct(null, do_get_list_domain_response, [callback]));	
			
			var fError 		= req_gl_funct(App, self.do_show_Msg, [$.i18n("common_err_ajax")]);	
			
			pr_domain_requesting = true;
			App.network.do_lc_ajax (App.path.BASE_URL_API, self.var_lc_URL_Aut_Header, ref, 100000, fSucces, fError) ;	
		}
		
		var do_get_list_domain_response = function(sharedJson, callback) {
			pr_domain_requesting = false;
			if(sharedJson[App['const'].SV_CODE] == App['const'].SV_CODE_API_YES) {
				var data = sharedJson[App['const'].RES_DATA];
				App.data.domains = data;
				do_gl_LocalStorage_Save("app_domains", data);
				
				if(callback) {
					callback(data);
				} 
			}
		}

		//--------------------------------------------------------------------------------------------
		this.do_show_Msg= function(sharedJson, msg){
			//alert(msg);
			console.log("do_show_Msg::" + msg);
		}		
			
	};

	return HomeMain;
  });