requirejs.config(requirejs_config);
var URL_API 	= '/bo/api';
var App = {	
		path : {
			BASE_URL_API					: URL_API, 

			API_SOR_ORDER					: "/ServiceTaSorOrder",
			
			API_PER_PERSON					: "/ServicePerPerson",
			API_AUT_USER					: "/ServiceAutUser",
			
			API_MAT_MATERIAL				: "/ServiceMatMaterial",
			API_MAT_UNIT					: "/ServiceMatUnit",
			
			API_LOGIN						: "/login",
			API_LOGOUT						: "/logout",
			
			API_UPLOAD						: "/up",
		
			DATA_FILE_URL 					: URL_API + "?" + "sv_class=servicetpydocument&sv_name=svtpydocumentgetfile&code=d_1_1728379486&pT=11000",
			LOCATION_URL_HREF				: '',
		},
	
		// Globals Constants routes
		router : {
			controller : null,
			routes : {				
				VI_MAIN						: "VI_MAIN"
			},
			part: {
				VI_AUT_USER					: "VI_AUT_USER",
				VI_MAT_MATERIAL				: "VI_MAT_MATERIAL",
				VI_MAT_BCODE				: "VI_MAT_BCODE",
				VI_MAT_UNIT     			: "VI_MAT_UNIT",
				VI_MAT_WARE_HOUSE  			: "VI_MAT_WARE_HOUSE",
				VI_PER_PERSON				: "VI_PER_PERSON",
				VI_PER_INFO				    : "VI_PER_INFO",
				VI_SOR_ORDER_IN				: "VI_SOR_ORDER_IN",
				VI_SOR_ORDER_OU				: "VI_SOR_ORDER_OU",
				VI_SOR_ORDER_IN_BUY			: "VI_SOR_ORDER_IN_BUY",
				VI_SOR_ORDER_IN_TRANSFERT	: "VI_SOR_ORDER_IN_TRANSFERT",
				VI_SOR_ORDER_IN_BAL			: "VI_SOR_ORDER_IN_BAL",
				VI_SOR_ORDER_OU_SELL		: "VI_SOR_ORDER_OU_SELL",
				VI_SOR_ORDER_OU_TRANSFERT	: "VI_SOR_ORDER_OU_TRANSFERT",
				VI_SOR_ORDER_OU_BAL			: "VI_SOR_ORDER_OU_BAL",
				VI_SOR_ORDER_OU_BAL_FAIL	: "VI_SOR_ORDER_OU_BAL_FAIL",
				VI_SOR_ORDER_OU_BAL_OTHER	: "VI_SOR_ORDER_OU_BAL_OTHER",
				VI_SOR_ORDER_OU_STAT		: "VI_SOR_ORDER_OU_STAT",
				VI_SOR_DEAL 				: "VI_SOR_DEAL",
				VI_VENDOR					: "VI_VENDOR",	
				VI_MAT_ALARM				: "VI_MAT_ALARM",
				VI_MAT_STOCK_RQ				: "VI_MAT_STOCK_RQ",
				VI_MAT_STOCK_RQ_MONTH		: "VI_MAT_STOCK_RQ_MONTH",
				
				VI_REPORT_SOR_ORDER_IN_WAREHOUSE		: "VI_REPORT_SOR_ORDER_IN_WAREHOUSE",	
				VI_REPORT_SOR_ORDER_OUT_WAREHOUSE		: "VI_REPORT_SOR_ORDER_OUT_WAREHOUSE",	
				
				
				VI_REPORT_SOR_ORDER_OUT_SELLING			: "VI_REPORT_SOR_ORDER_OUT_SELLING",	
				VI_REPORT_SOR_ORDER_OUT_SELLING_ANALYZE	: "VI_REPORT_SOR_ORDER_OUT_SELLING_ANALYZE",	
				
				VI_TPY_CATEGORY				: "VI_TPY_CATEGORY",
				
				VI_NSO_POST					: "VI_NSO_POST",
				VI_NSO_OFFER				: "VI_NSO_OFFER",
				
				
				VI_AUT_RIGHT				: "VI_AUT_RIGHT",
				VI_AUT_ROLE					: "VI_AUT_ROLE",
			}
		},
		controller: {
		},
		template: {			
			names:{
				
			}
		},
		'const':{	


		},
		data:{
			
		},
		varname:{
			
		},
		funct :{    	
			
		},
		constHTML :{    	
			
		},
};

var bib 		= ['jquery', 
	
				   'main/dashboard/ctrl/RouteController',
				   
		           'common/ctrl/Network',  
		           'common/ctrl/TemplateController',           
		           'common/ctrl/MsgboxController_New',
		           'common/ctrl/SummerNoteController',
		           'common/ctrl/RightController',
		           'common/ctrl/FileURLController',
		           'common/ctrl/CameraController',
		           
		           'sha256.min'
		           ];

var bib_all = [...bib, ...bib_common, ...bib_hnv_tool, ...bib_prj, ...bib_transl, ...bib_others, ...bib_secu, ...bib_datetime, ...bib_summernote];

if(can_gl_MobileOrTablet())	bib_all = [...bib_all, ...bib_cordova];

//Start the main app logic.
requirejs(bib_all, function ($, RouteController, Network, TemplateController, MsgboxController, SummerNoteController, RightController, FileURLController, CameraController ) {
	$(document).ready(function() {
		//--------------Begin app here ----------------------------------------------------------------------------------------------	
		do_gl_InitApp(RouteController, Network, TemplateController, MsgboxController, SummerNoteController, RightController, FileURLController, CameraController);
		App.data.url 	= decodeURIComponent(window.location.search.substring(1));
	});	
});
//------------------------------------------------------------------------------